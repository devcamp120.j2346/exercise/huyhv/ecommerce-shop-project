import { NavLink, useNavigate } from "react-router-dom";
import Subcribe from "../../components/HomePage/Subcribe";
import Breadcrumb from "../../components/Breadcrumb/Breadcrumb";
import { IoHomeOutline } from "react-icons/io5";
import { useState } from "react";
import { fetchApiLoginAction } from '../../store/actions/apiRequest';
import { useDispatch, useSelector } from "react-redux";

const listCrumbs = [
  { name: <IoHomeOutline className='text-[20px]' />, url: '/' },
  { name: 'Account', url: '/account' },
  { name: 'Login', url: '/login' },
]

const SignInPage = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const handleSignIn = (e) => {
    e.preventDefault();

    const newUser = {
      username: username.trim(),
      password: password.trim()
    }

    dispatch(fetchApiLoginAction(newUser, navigate))
  }

  return (
    <>
      <Breadcrumb crumbs={listCrumbs} />
      <div className="max-w-screen-xl mx-auto px-4">
        <form
          className="bg-white px-6 pt-6 pb-8 rounded-lg shadow-lg flex flex-col gap-5 max-w-lg mx-auto my-20"
          onSubmit={handleSignIn}
        >
          <h2 className="text-gray-900 text-[32px] font-semibold leading-[39px] text-center">Sign In</h2>
          <div>
            <input
              type="text"
              placeholder="Username"
              className="leading-[21px] px-4 py-[14px] border rounded-md w-full outline-none"
              onChange={(e) => setUsername(e.target.value)}
            />
          </div>
          <div>
            <input
              type="password"
              placeholder="Password"
              className="leading-[21px] px-4 py-[14px] border rounded-md w-full outline-none"
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>

          <div className="flex flex-col md:flex-row justify-between gap-y-3 items-center">
            {/* Remember Me */}
            <div className="flex items-center gap-[6px]">
              <input type="checkbox" />
              <p className="text-gray-600 text-[14px] leading-[21px]">Remember me</p>
            </div>

            {/* Forgor Link */}
            <a href="/forgot" className="text-gray-600 text-[14px] leading-[21px]">Forget Password</a>
          </div>

          <button className="text-white bg-[#00B207] text-[14px] font-semibold leading-4 py-[14px] rounded-full">Login</button>

          <p className="text-gray-600 text-[14px] leading-[21px] text-center">
            Don't have account?
            <NavLink to="/signup" className="text-gray-900 font-medium"> Register</NavLink>
          </p>
        </form>
      </div>
      <Subcribe />
    </>

  );
};

export default SignInPage;