import { useParams } from "react-router-dom";
import { IoHomeOutline } from "react-icons/io5";
import { useEffect } from "react";
import { fetchApiProductByIdAction } from '../../store/actions/apiRequest'
import { useDispatch, useSelector } from "react-redux";
import Breadcrumb from '../../components/Breadcrumb/Breadcrumb';
import ProductDetail from '../../components/DetailPage/ProductDetail';
import DescriptionProduct from "../../components/DetailPage/DescriptionProduct";
import Subcribe from "../../components/HomePage/Subcribe";


const DetailPage = () => {
  const dispatch = useDispatch();
  const param = useParams();
  const { productById } = useSelector(reduxData => reduxData.productReducer);

  const { product } = productById;
  const { id } = param;

  useEffect(() => {
    dispatch(fetchApiProductByIdAction(id));
  }, [])

  // Breadcrumb variable
  const listCrumbs = [
    { name: <IoHomeOutline className='text-[20px]' />, url: '/' },
    { name: 'Shop', url: '/shop' },
    { name: `${product.data?.name}`, url: '/' },
  ]

  return (
    <div>
      <Breadcrumb crumbs={listCrumbs} />
      <ProductDetail product={product.data} />
      <DescriptionProduct />
      <Subcribe />
    </div>
  );
};

export default DetailPage;