import Breadcrumb from '../../components/Breadcrumb/Breadcrumb';
import { IoHomeOutline } from "react-icons/io5";
import BillInformation from '../../components/CheckoutPage/BillInformation';
import OrderSummary from '../../components/CheckoutPage/OrderSummary';
import Subcribe from '../../components/HomePage/Subcribe';

// Breadcrumb variable
const listCrumbs = [
  { name: <IoHomeOutline className='text-[20px]' />, url: '/' },
  { name: 'Shopping Cart', url: '/cart' },
  { name: 'Checkout', url: '/checkout' },
]

const CheckoutPage = () => {
  return (
    <>
      <Breadcrumb crumbs={listCrumbs} />
      <div className='max-w-screen-xl mx-auto px-4 mt-8 mb-20 flex flex-col lg:flex-row gap-6'>
        <BillInformation />
        <OrderSummary />
      </div>
      <Subcribe />
    </>
  );
};

export default CheckoutPage;