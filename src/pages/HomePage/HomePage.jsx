import HomeSlider from "../../components/HomePage/HomeSlider";
import Service from '../../components/HomePage/Service';
import OurProduct from '../../components/HomePage/OurProduct';
import Promotion from '../../components/HomePage/Promotion'
import Deals from "../../components/HomePage/Deals";
import Feature from "../../components/HomePage/Feature";
import Client from "../../components/HomePage/Client";
import FollowUs from "../../components/HomePage/FollowUs";
import Subcribe from "../../components/HomePage/Subcribe";

const HomePage = () => {
  return (
    <>
      <HomeSlider />
      <Service />
      <OurProduct />
      <Promotion />
      <Deals />
      <Feature />
      <Client />
      <FollowUs />
      <Subcribe />
    </>
  );
};

export default HomePage;