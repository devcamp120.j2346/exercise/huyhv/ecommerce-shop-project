import { IoHomeOutline } from "react-icons/io5";
import Breadcrumb from '../../components/Breadcrumb/Breadcrumb';
import ShopCart from "../../components/ShoppingCartPage/ShopCart";
import Subcribe from '../../components/HomePage/Subcribe';

// Breadcrumb variable
const listCrumbs = [
  { name: <IoHomeOutline className='text-[20px]' />, url: '/' },
  { name: 'Shopping Cart', url: '/cart' },
]

const ShoppingCartPage = () => {
  return (
    <>
      <Breadcrumb crumbs={listCrumbs} />
      <ShopCart />
      <Subcribe />
    </>
  );
};

export default ShoppingCartPage;