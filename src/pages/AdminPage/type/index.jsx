import { useEffect } from "react";
import { Box, useTheme, Button } from "@mui/material";
import { DataGrid, GridToolbar } from "@mui/x-data-grid";
import { tokens } from "../../../theme";
import Header from "../../../components/AdminPage/Header";
import { fetchApiProductTypeAction } from '../../../store/actions/admin/apiRequestAdmin';
import { useDispatch, useSelector } from "react-redux";
import { handleOpenDetailModalTypeAction, handleOpenDeleteModalTypeAction } from '../../../store/actions/admin/modalAdmin';
import ModalDetailType from "../../../components/AdminPage/ModalDetailType";
import ModalDeleteType from "../../../components/AdminPage/ModalDeleteType";

const Type = () => {
  const dispatch = useDispatch();
  const theme = useTheme();
  const colors = tokens(theme.palette.mode);

  const { productType } = useSelector(reduxData => reduxData.adminReducer);
  const { pending, listProductType, isError } = productType;

  useEffect(() => {
    dispatch(fetchApiProductTypeAction())
  }, [])

  const handleViewDetail = (info) => {
    dispatch(handleOpenDetailModalTypeAction(info));
  }

  const handleDelete = (info) => {
    dispatch(handleOpenDeleteModalTypeAction(info));
  }

  const columns = [
    {
      field: "_id",
      headerName: "ID",
    },
    {
      field: "name",
      headerName: "Name",
      flex: 1,
      cellClassName: "name-column--cell"
    },
    {
      field: "description",
      headerName: "Description",
      flex: 1,
    },
    {
      field: "action",
      headerName: "Actions",
      flex: 1,
      renderCell: ({ row }) => {
        return (
          <Box
            width="60%"
            m="0 auto"
            p="5px"
            display="flex"
            justifyContent="center"
            gap="1rem"
          >
            <Button
              variant="contained"
              color="success"
              onClick={() => handleViewDetail(row)}
            >
              View Detail
            </Button>
            <Button
              variant="contained"
              color="error"
              onClick={() => handleDelete(row)}
            >
              Delete
            </Button>
          </Box>
        )
      }
    },
  ]

  return (
    <Box m="20px">
      <Header title="PRODUCT TYPES" subtitle="Managing List Products Type" />

      <Box
        m="40px 0 0 0"
        height="75vh"
        sx={{
          "& .MuiDataGrid-root": {
            border: "none",
          },
          "& .MuiDataGrid-cell": {
            borderBottom: "none"
          },
          "& .name-column--cell": {
            color: colors.greenAccent[300]
          },
          "& .MuiDataGrid-columnHeaders": {
            backgroundColor: colors.blueAccent[700],
            borderBottom: "none"
          },
          "& .MuiDataGrid-virtualScroller": {
            backgroundColor: colors.primary[400],
          },
          "& .MuiDataGrid-footerContainer": {
            borderTop: "none",
            backgroundColor: colors.blueAccent[700],
          },
          "& .MuiDataGrid-toolbarContainer .MuiButton-text": {
            color: `${colors.grey[100]} !important`
          }
        }}
      >
        {
          listProductType ?
            (
              <DataGrid
                rows={listProductType?.data}
                columns={columns}
                getRowId={row => row._id}
                components={{ Toolbar: GridToolbar }}
              />
            )
            :
            (<></>)
        }
      </Box>

      <ModalDetailType />
      <ModalDeleteType />
    </Box>
  );
};

export default Type;