import { Box } from "@mui/material";
import Header from '../../../components/AdminPage/Header';
import PieChart from "../../../components/AdminPage/PieChart";


const Pie = () => {
  return (
    <Box m="20px">
      <Header title="Pie Chart" subtitle="Simple Pie Chart" />

      <Box height="75vh">
        <PieChart />
      </Box>
    </Box>
  );
};

export default Pie;