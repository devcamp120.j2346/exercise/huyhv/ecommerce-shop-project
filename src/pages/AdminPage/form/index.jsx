import { Box, Button, TextField, InputLabel, FormControl, Select, MenuItem } from "@mui/material";
import { Formik } from "formik";
import * as yup from 'yup';
import { useMediaQuery } from "@mui/material";
import Header from '../../../components/AdminPage/Header';
import { useEffect } from "react";
import { fetchApiProductTypeAction, fetchApiCreateProductAction } from '../../../store/actions/admin/apiRequestAdmin';
import { useDispatch, useSelector } from "react-redux";

const initialValues = {
  name: '',
  type: '',
  description: '',
  imageUrl: '',
  buyPrice: '',
  promotionPrice: '',
  amount: '',
}

const userSchema = yup.object().shape({
  name: yup.string().required("Name is Required"),
  type: yup.string().required("Type is required"),
  description: yup.string().required("Description is required"),
  imageUrl: yup
    .string()
    // .email("invalid email")
    .required("Image is required"),
  buyPrice: yup
    .string()
    // .matches(phoneRegExp, "Phone number is not valid")
    .required("Buy Price is required"),
  promotionPrice: yup.string().required("Promotion Price is required"),
  amount: yup.string().required("Amount is required"),
})

const Form = () => {
  const dispatch = useDispatch();
  const { productType } = useSelector(reduxData => reduxData.adminReducer);
  const isNonMobile = useMediaQuery("(min-width:600px)");

  useEffect(() => {
    dispatch(fetchApiProductTypeAction());
  }, [])

  const { pending, isError, listProductType } = productType;

  const handleFormSubmit = (values) => {
    dispatch(fetchApiCreateProductAction(values));
  }

  return (
    <Box m="20px">
      <Header title="CREATE PRODUCT" subtitle="Create a New Product" />

      <Formik
        onSubmit={handleFormSubmit}
        initialValues={initialValues}
        validationSchema={userSchema}
      >
        {({ values, errors, touched, handleBlur, handleChange, handleSubmit }) => (
          <form onSubmit={handleSubmit}>
            <Box
              display="grid"
              gap="30px"
              gridTemplateColumns="repeat(4, minmax(0, 1fr))"
              sx={{
                "& > div": { gridColumn: isNonMobile ? undefined : "span 4" },
              }}
            >
              <TextField
                fullWidth
                variant="filled"
                type="text"
                label="Product Name"
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.name}
                name="name"
                error={!!touched.name && !!errors.name}
                helperText={touched.name && errors.name}
                sx={{ gridColumn: "span 2" }}
              />

              <FormControl
                sx={{ gridColumn: "span 2" }}
              >
                <InputLabel>Product Type</InputLabel>
                <Select
                  label="Product Type"
                  variant="filled"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.type}
                  name="type"
                  error={!!touched.type && !!errors.type}
                  helpertext={touched.type && errors.type}
                  defaultValue=""
                  fullWidth
                >
                  {listProductType?.data.map(item => (
                    <MenuItem key={item._id} value={item._id}>{item.name}</MenuItem>

                  ))}
                </Select>
              </FormControl>

              <TextField
                fullWidth
                variant="filled"
                type="text"
                label="Description"
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.description}
                name="description"
                error={!!touched.description && !!errors.description}
                helperText={touched.description && errors.description}
                sx={{ gridColumn: "span 4" }}
              />
              <TextField
                fullWidth
                variant="filled"
                type="text"
                label="Image Url"
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.imageUrl}
                name="imageUrl"
                error={!!touched.imageUrl && !!errors.imageUrl}
                helperText={touched.imageUrl && errors.imageUrl}
                sx={{ gridColumn: "span 4" }}
              />
              <TextField
                fullWidth
                variant="filled"
                type="text"
                label="Buy Price"
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.buyPrice}
                name="buyPrice"
                error={!!touched.buyPrice && !!errors.buyPrice}
                helperText={touched.buyPrice && errors.buyPrice}
                sx={{ gridColumn: "span 4" }}
              />
              <TextField
                fullWidth
                variant="filled"
                type="text"
                label="Promotion Price"
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.promotionPrice}
                name="promotionPrice"
                error={!!touched.promotionPrice && !!errors.promotionPrice}
                helperText={touched.promotionPrice && errors.promotionPrice}
                sx={{ gridColumn: "span 4" }}
              />
              <TextField
                fullWidth
                variant="filled"
                type="text"
                label="Amount"
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.amount}
                name="amount"
                error={!!touched.amount && !!errors.amount}
                helperText={touched.amount && errors.amount}
                sx={{ gridColumn: "span 4" }}
              />
            </Box>

            <Box display="flex" justifyContent="end" mt="20px">
              <Button type="submit" color="secondary" variant="contained">
                Create New Product
              </Button>
            </Box>
          </form>
        )}
      </Formik>
    </Box>
  );
};

export default Form;