import { Box, useTheme, Button, Typography } from "@mui/material";
import { DataGrid, GridToolbar } from "@mui/x-data-grid";
import { tokens } from "../../../theme";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { fetchApiProductsAction } from '../../../store/actions/admin/apiRequestAdmin';
import Header from '../../../components/AdminPage/Header';
import ModalDetailProduct from "../../../components/AdminPage/ModalDetailProduct";
import ModalDeleteProduct from "../../../components/AdminPage/ModalDeleteProduct";
import { handleOpenDetailModalAction, handleOpenDeleteModalAction } from '../../../store/actions/admin/modalAdmin';

const Products = () => {
  const dispatch = useDispatch();
  const { product } = useSelector(reduxData => reduxData.adminReducer);
  const theme = useTheme();
  const colors = tokens(theme.palette.mode);
  const { pending, listProduct, isError } = product;

  const handleViewDetail = (row) => {
    dispatch(handleOpenDetailModalAction(row));
  }

  const handleDelete = (row) => {
    dispatch(handleOpenDeleteModalAction(row));
  }

  useEffect(() => {
    dispatch(fetchApiProductsAction());
  }, [])

  const columns = [
    {
      field: "_id",
      headerName: "ID",
    },
    {
      field: "name",
      headerName: "Name",
      flex: 1,
      cellClassName: "name-column--cell"
    },
    {
      field: "amount",
      headerName: "Amount",
      type: "number",
      headerAlign: "left",
      align: "left",
    },
    {
      field: "buyPrice",
      headerName: "Buy Price",
      type: "number",
      headerAlign: "left",
      align: "left",
    },
    {
      field: "promotionPrice",
      headerName: "Promotion Price",
      type: "number",
      headerAlign: "left",
      align: "left",
    },
    {
      field: "description",
      headerName: "Description",
      flex: 1,
    },
    {
      field: "action",
      headerName: "Actions",
      flex: 1,
      renderCell: ({ row }) => {
        return (
          <Box
            width="60%"
            m="0 auto"
            p="5px"
            display="flex"
            justifyContent="center"
            gap="1rem"
          >
            <Button
              variant="contained"
              color="success"
              onClick={() => handleViewDetail(row)}
            >
              View Detail
            </Button>
            <Button
              variant="contained"
              color="error"
              onClick={() => handleDelete(row)}
            >
              Delete
            </Button>
          </Box>
        )
      }
    },
  ]

  return (
    <Box m="20px">
      <Header title="PRODUCTS" subtitle="Managing List Products" />
      <Box
        m="40px 0 0 0"
        height="75vh"
        sx={{
          "& .MuiDataGrid-root": {
            border: "none",
          },
          "& .MuiDataGrid-cell": {
            borderBottom: "none"
          },
          "& .name-column--cell": {
            color: colors.greenAccent[300]
          },
          "& .MuiDataGrid-columnHeaders": {
            backgroundColor: colors.blueAccent[700],
            borderBottom: "none"
          },
          "& .MuiDataGrid-virtualScroller": {
            backgroundColor: colors.primary[400],
          },
          "& .MuiDataGrid-footerContainer": {
            borderTop: "none",
            backgroundColor: colors.blueAccent[700],
          },
          "& .MuiDataGrid-toolbarContainer .MuiButton-text": {
            color: `${colors.grey[100]} !important`
          }
        }}
      >
        {
          listProduct ?
            (
              <DataGrid
                rows={listProduct?.data}
                columns={columns}
                getRowId={row => row._id}
                components={{ Toolbar: GridToolbar }}
              />
            )
            :
            (<></>)
        }
      </Box>

      <ModalDetailProduct />
      <ModalDeleteProduct />
    </Box>
  );
};

export default Products;