import { Box, useTheme, Button } from "@mui/material";
import { DataGrid, GridToolbar } from "@mui/x-data-grid";
import { tokens } from "../../../theme";
import Header from "../../../components/AdminPage/Header";
import { fetchApiCustomersAction } from '../../../store/actions/admin/apiRequestAdmin';
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { handleOpenDetailModalCustomerAction, handleOpenDeleteModalCustomerAction } from '../../../store/actions/admin/modalAdmin';
import ModalDetailCustomer from '../../../components/AdminPage/ModalDetailCustomer';
import ModalDeleteCustomer from "../../../components/AdminPage/ModalDeleteCustomer";

const Customers = () => {
  const dispatch = useDispatch();
  const { customer } = useSelector(reduxData => reduxData.adminReducer);
  const theme = useTheme();
  const colors = tokens(theme.palette.mode);

  useEffect(() => {
    dispatch(fetchApiCustomersAction());
  }, [])

  const { pending, isError, listCustomers } = customer;

  const handleViewDetail = (info) => {
    dispatch(handleOpenDetailModalCustomerAction(info))
  }
  const handleDelete = (info) => {
    dispatch(handleOpenDeleteModalCustomerAction(info));
  }

  const columns = [
    {
      field: "_id",
      headerName: "ID",
    },
    {
      field: "fullName",
      headerName: "Full Name",
      flex: 1,
      cellClassName: "name-column--cell"
    },
    {
      field: "phone",
      headerName: "Phone Number",
      flex: 1
    },
    {
      field: "email",
      headerName: "Email",
      flex: 1
    },
    {
      field: "address",
      headerName: "Address",
      flex: 1
    },
    {
      field: "city",
      headerName: "City",
      flex: 1
    },
    {
      field: "country",
      headerName: "Country",
      flex: 1
    },
    {
      field: "action",
      headerName: "Actions",
      flex: 1,
      renderCell: ({ row }) => {
        return (
          <Box
            width="60%"
            m="0 auto"
            p="5px"
            display="flex"
            justifyContent="center"
            gap="1rem"
          >
            <Button
              variant="contained"
              color="success"
              onClick={() => handleViewDetail(row)}
            >
              View Detail
            </Button>
            <Button
              variant="contained"
              color="error"
              onClick={() => handleDelete(row)}
            >
              Delete
            </Button>
          </Box>
        )
      }
    },
  ]

  return (
    <Box m="20px">
      <Header title="PRODUCT TYPES" subtitle="Managing List Products Type" />

      <Box
        m="40px 0 0 0"
        height="75vh"
        sx={{
          "& .MuiDataGrid-root": {
            border: "none",
          },
          "& .MuiDataGrid-cell": {
            borderBottom: "none"
          },
          "& .name-column--cell": {
            color: colors.greenAccent[300]
          },
          "& .MuiDataGrid-columnHeaders": {
            backgroundColor: colors.blueAccent[700],
            borderBottom: "none"
          },
          "& .MuiDataGrid-virtualScroller": {
            backgroundColor: colors.primary[400],
          },
          "& .MuiDataGrid-footerContainer": {
            borderTop: "none",
            backgroundColor: colors.blueAccent[700],
          },
          "& .MuiDataGrid-toolbarContainer .MuiButton-text": {
            color: `${colors.grey[100]} !important`
          }
        }}
      >
        {
          listCustomers ?
            (
              <DataGrid
                rows={listCustomers?.data}
                columns={columns}
                getRowId={row => row._id}
                components={{ Toolbar: GridToolbar }}
              />
            )
            :
            (<></>)
        }
      </Box>

      <ModalDetailCustomer />
      <ModalDeleteCustomer />
    </Box>
  );
};

export default Customers;