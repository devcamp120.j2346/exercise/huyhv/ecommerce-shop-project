import Breadcrumb from "../../components/Breadcrumb/Breadcrumb";
import Subcribe from "../../components/HomePage/Subcribe";
import Sale from "../../components/ShopPage/Sale";
import Shop from "../../components/ShopPage/Shop";
import { IoHomeOutline } from "react-icons/io5";

// Breadcrumb variable
const listCrumbs = [
  { name: <IoHomeOutline className='text-[20px]' />, url: '/' },
  { name: 'Shop', url: '/shop' },
]

const ShopPage = () => {
  return (
    <>
      <Breadcrumb crumbs={listCrumbs} />
      <Sale />
      <Shop />
      <Subcribe />
    </>
  );
};

export default ShopPage;