import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import './index.css'

// Import Page
import HomePage from './pages/HomePage/HomePage';
import ShopPage from './pages/ShopPage/ShopPage';
import AboutPage from './pages/AboutPage/AboutPage';
import ContactPage from './pages/ContactPage/ContactPage';
import SignInPage from './pages/SignInPage/SignInPage';
import SignUpPage from './pages/SignUpPage/SignUpPage';
import NotFoundPage from './pages/NotFoundPage/NotFoundPage';
import DetailPage from './pages/DetailPage/DetailPage';
import ShoppingCartPage from './pages/ShoppingCartPage/ShoppingCartPage';
import CheckoutPage from './pages/CheckoutPage/CheckoutPage';
import Form from './pages/AdminPage/form';

// Import Admin Page
import AdminPage from './pages/AdminPage/AdminPage';
import Dashboard from './pages/AdminPage/dashboard';
import Products from './pages/AdminPage/products';
import Page404 from './pages/AdminPage/404page';
import Type from './pages/AdminPage/type';
import Customers from './pages/AdminPage/customer';
import Calendar from './pages/AdminPage/calendar';
import Bar from './pages/AdminPage/bar';
import Pie from './pages/AdminPage/pie';
import Line from './pages/AdminPage/line';
import Geography from './pages/AdminPage/geography';

// Import React Redux Store
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import { createStore, applyMiddleware } from 'redux';
import rootReducer from './store/reducer';
import { thunk } from "redux-thunk";
import { Provider } from 'react-redux'

const store = createStore(rootReducer, applyMiddleware(thunk));

const router = createBrowserRouter([
  {
    path: '/',
    element: <App />,
    children: [
      {
        index: true,
        element: <HomePage />
      },
      {
        path: '/shop',
        element: <ShopPage />
      },
      {
        path: '/shop/:id',
        element: <DetailPage />
      },
      {
        path: '/cart',
        element: <ShoppingCartPage />
      },
      {
        path: '/checkout',
        element: <CheckoutPage />
      },
      {
        path: '/about',
        element: <AboutPage />
      },
      {
        path: '/contact',
        element: <ContactPage />
      },
      {
        path: '/login',
        element: <SignInPage />
      },
      {
        path: '/signup',
        element: <SignUpPage />
      },
      {
        path: '/*',
        element: <NotFoundPage />
      },
    ]
  },
  {
    path: '/admin',
    element: <AdminPage />,
    children: [
      {
        index: true,
        element: <Dashboard />
      },
      {
        path: '/admin/products',
        element: <Products />
      },
      {
        path: '/admin/types',
        element: <Type />
      },
      {
        path: '/admin/customers',
        element: <Customers />
      },
      {
        path: '/admin/create-product',
        element: <Form />
      },
      {
        path: '/admin/calendar',
        element: <Calendar />
      },
      {
        path: '/admin/bar',
        element: <Bar />
      },
      {
        path: '/admin/pie',
        element: <Pie />
      },
      {
        path: '/admin/line',
        element: <Line />
      },
      {
        path: '/admin/geography',
        element: <Geography />
      },
      {
        path: '/admin/*',
        element: <Page404 />
      },
    ]
  }
])

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <Provider store={store}>
      <RouterProvider router={router} />
    </Provider>
  </React.StrictMode>,
)
