import CardProduct from "../Product/CardProduct";
import { useDispatch, useSelector } from "react-redux";
import { fetchApiProductAction } from '../../store/actions/apiRequest';
import { useEffect } from "react";

const Feature = () => {
  const dispatch = useDispatch();
  const { product } = useSelector(reduxData => reduxData.productReducer)
  const { listProduct } = product;
  // console.log(listProduct)

  useEffect(() => {
    dispatch(fetchApiProductAction())
  }, [])

  return (
    <>
      <div className="max-w-screen-xl mx-auto my-[100px]">
        <h2 className="text-[#002603] text-[40px] font-semibold leading-[48px] mb-[50px] text-center">Featured Products</h2>

        <div className="grid grid-cols-1 gap-2 md:grid-cols-2 lg:grid-cols-4 px-4">
          {listProduct && listProduct?.length > 0 &&
            listProduct?.map((product, index) => {
              if (index < 4) {
                return (
                  <CardProduct
                    key={product._id}
                    name={product.name}
                    imageUrl={product.imageUrl}
                    buyPrice={product.buyPrice}
                    promotionPrice={product.promotionPrice}
                  />
                )
              }
            })}
        </div>
      </div>
    </>
  );
};

export default Feature;