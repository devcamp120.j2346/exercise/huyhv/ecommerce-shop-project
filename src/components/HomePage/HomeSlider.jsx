import Slider from "react-slick";
import Hero from "./Hero";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import '../../styles/slider.css';

const HomeSlider = () => {
  const settings = {
    dots: true,
    infinite: true,
    autoplay: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 600,
        settings: {
          dots: false,
        }
      }
    ]
  };

  return (
    <div>
      <div className="max-w-screen-2xl mx-auto px-7 pt-16 pb-40">
        <Slider {...settings}>
          <Hero />
          <Hero />
          <Hero />
        </Slider>
      </div>
    </div>

  );
};

export default HomeSlider;