import CardProduct from "../Product/CardProduct";
import { useDispatch, useSelector } from "react-redux";
import { fetchApiProductTypeAction, fetchApiProductAction } from '../../store/actions/apiRequest'
import { NavLink } from "react-router-dom";
import { useEffect } from "react";

const OurProduct = () => {
  const dispatch = useDispatch();
  const { productType, product } = useSelector(reduxData => reduxData.productReducer);

  const { listProductType } = productType;
  const { listProduct } = product;

  useEffect(() => {
    dispatch(fetchApiProductTypeAction());
  }, [])

  useEffect(() => {
    dispatch(fetchApiProductAction());
  }, [])

  const handleClickProductType = (productTypeId) => {
    dispatch(fetchApiProductAction(productTypeId))
  }

  return (
    <div className="bg-[#EDF2EE] px-4">
      {/* Title */}
      <h2 className="text-gray-900 text-[40px] font-semibold leading-[48px] text-center pt-[211px] mb-[24px]">Introducing Our Products</h2>


      <ul className="flex justify-center items-center mb-[50px]">

        {/* View All */}
        <li className="text-gray-500 font-medium leading-6 cursor-pointer border-r border-[#B4CCB4] px-6" onClick={() => handleClickProductType()}>
          All
        </li>

        {/* List Categories */}
        {listProductType && listProductType.data?.length > 0 &&
          listProductType.data?.map(productType => (
            <li
              key={productType._id}
              className="text-gray-500 font-medium leading-6 cursor-pointer border-r border-[#B4CCB4] px-6"
              onClick={() => handleClickProductType(productType._id)}
            >
              {productType.name}
            </li>
          ))}

        {/* Navigate to List Product Page */}
        <li className="text-gray-500 font-medium leading-6 cursor-pointer px-6">
          <NavLink to="/shop">View All</NavLink>
        </li>
      </ul>

      {/* List Product */}
      <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 max-w-screen-xl mx-auto gap-2 pb-[100px]">

        {/* Just render 8 items */}
        {listProduct && listProduct?.length > 0 &&
          listProduct?.map((product, index) => {
            if (index < 8) {
              return (
                <CardProduct
                  key={product._id}
                  name={product.name}
                  imageUrl={product.imageUrl}
                  buyPrice={product.buyPrice}
                  promotionPrice={product.promotionPrice}
                />
              )
            }
          })}

      </div>
    </div>

  );
};

export default OurProduct;