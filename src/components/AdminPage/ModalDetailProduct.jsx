import {
  Typography,
  Modal,
  Button,
  Box,
  TextField,
  Select,
  MenuItem,
  InputLabel,
  FormControl
} from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { handleCloseDetailModalAction } from '../../store/actions/admin/modalAdmin';
import {
  handleChangeAmountAction,
  handleChangeBuyPriceAction,
  handleChangeDescAction,
  handleChangeImageAction,
  handleChangeNameAction,
  handleChangePromotionPriceAction,
  handleChangeTypeAction
} from '../../store/actions/admin/changeInput';
import {
  fetchApiUpdateProductByIdAction,
  fetchApiProductTypeAction
} from '../../store/actions/admin/apiRequestAdmin';
import { useEffect } from "react";

const ModalDetailProduct = () => {
  const dispatch = useDispatch();
  const { detailModal, productType } = useSelector(reduxData => reduxData.adminReducer);
  const {
    open,
    id,
    name,
    description,
    imageUrl,
    buyPrice,
    promotionPrice,
    type,
    amount,
  } = detailModal;

  // Close Modal
  const handleClose = () => {
    dispatch(handleCloseDetailModalAction());
  }

  // Change Info
  const handleChangeName = (e) => {
    dispatch(handleChangeNameAction(e.target.value));
  }
  const handleChangeDesc = (e) => {
    dispatch(handleChangeDescAction(e.target.value));
  }
  const handleChangeType = (e) => {
    dispatch(handleChangeTypeAction(e.target.value));
  }
  const handleChangeImage = (e) => {
    dispatch(handleChangeImageAction(e.target.value));
  }
  const handleChangeBuyPrice = (e) => {
    dispatch(handleChangeBuyPriceAction(e.target.value));
  }
  const handleChangePromotionPrice = (e) => {
    dispatch(handleChangePromotionPriceAction(e.target.value));
  }
  const handleChangeAmount = (e) => {
    dispatch(handleChangeAmountAction(e.target.value));
  }

  // Update Product
  const newInfoProduct = {
    name,
    description,
    imageUrl,
    buyPrice,
    promotionPrice,
    type,
    amount
  }

  // Update Product Event
  const handleUpdateProduct = () => {
    dispatch(fetchApiUpdateProductByIdAction(id, newInfoProduct));
  }

  // Style for Modal
  const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 800,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
  };

  // Fetch Product Type
  useEffect(() => {
    dispatch(fetchApiProductTypeAction());
  }, [])

  return (
    <>
      {/* Modal View Detail */}
      <Modal
        open={open}
        onClose={handleClose}
      >
        <Box sx={style}>
          <Typography variant="h4" component="h2" sx={{ mb: 2 }}>
            Detail Product
          </Typography>

          {/* ID & Name */}
          <Box display="flex" gap="15px" marginBottom='15px'>
            <TextField
              label="ID"
              variant="outlined"
              fullWidth
              disabled
              value={id}
            />
            <TextField
              label="Name"
              variant="outlined"
              fullWidth
              value={name}
              onChange={(e) => handleChangeName(e)}
            />
          </Box>

          {/* Description */}
          <Box marginBottom="15px">
            <TextField
              label="Description"
              variant="outlined"
              fullWidth
              value={description}
              onChange={(e) => handleChangeDesc(e)}
            />
          </Box>

          {/* Buy Price & Promotion Price */}
          <Box display="flex" gap="15px" marginBottom="15px">
            <TextField
              label="Buy Price"
              variant="outlined"
              fullWidth
              value={buyPrice}
              onChange={(e) => handleChangeBuyPrice(e)}
            />
            <TextField
              label="Promotion Price"
              variant="outlined"
              fullWidth
              value={promotionPrice}
              onChange={(e) => handleChangePromotionPrice(e)}
            />
          </Box>

          {/* Amount & Type */}
          <Box display="flex" gap="15px" marginBottom="15px">
            <TextField
              label="Amount"
              variant="outlined"
              fullWidth
              value={amount}
              onChange={(e) => handleChangeAmount(e)}
            />

            {/* Select */}
            <FormControl fullWidth>
              <InputLabel>Type</InputLabel>
              <Select
                value={type}
                label="Type"
                variant="outlined"
                defaultValue=""
                onChange={(e) => handleChangeType(e)}
              >
                {productType.listProductType?.data.map(item => (
                  <MenuItem
                    key={item._id}
                    value={item._id}
                  >
                    {item.name}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </Box>

          {/* Image Url */}
          <Box>
            <TextField
              label="Image Url"
              variant="outlined"
              fullWidth
              value={imageUrl}
              onChange={(e) => handleChangeImage(e)}
            />
          </Box>
          <Box display="flex" justifyContent="center" marginBottom="15px">
            <img src={imageUrl} className="w-80 object-cover" />
          </Box>

          {/* Actions */}
          <Box display="flex" gap="15px" justifyContent="flex-end">
            <Button
              variant="contained"
              color="success"
              onClick={() => handleUpdateProduct()}
            >
              Update
            </Button>
            <Button
              variant="contained"
              color="error"
              onClick={() => handleClose()}
            >
              Cancel
            </Button>
          </Box>
        </Box>
      </Modal>
    </>
  );
};

export default ModalDetailProduct;