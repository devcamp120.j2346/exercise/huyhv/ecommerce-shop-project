import { Box, Typography, Modal, Button, TextField } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { handleCloseDetailModalTypeAction } from '../../store/actions/admin/modalAdmin';
import {
  handleChangeNameTypeAction,
  handleChangeDescTypeAction
} from '../../store/actions/admin/changeInput';
import { fetchApiUpdateProductTypeByIdAction } from '../../store/actions/admin/apiRequestAdmin';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 800,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

const ModalDetailType = () => {
  const dispatch = useDispatch();
  const { detailModalType } = useSelector(reduxData => reduxData.adminReducer);
  const { open, id, name, description } = detailModalType;

  const handleClose = () => {
    dispatch(handleCloseDetailModalTypeAction());
  }

  const handleChangeName = (e) => {
    dispatch(handleChangeNameTypeAction(e.target.value));
  }
  const handleChangeDesc = (e) => {
    dispatch(handleChangeDescTypeAction(e.target.value));
  }

  // Update Product Type
  const newInfoProductType = {
    name,
    description
  }
  const handleUpdateType = () => {
    dispatch(fetchApiUpdateProductTypeByIdAction(id, newInfoProductType));
  }

  return (
    <>
      <Modal
        open={open}
        onClose={handleClose}
      >
        <Box sx={style}>
          <Typography variant="h4" component="h2" sx={{ mb: 2 }}>
            Product Type Detail
          </Typography>

          {/* ID & Name */}
          <Box
            display="flex"
            gap="15px"
            marginBottom="15px"
          >
            <TextField
              label="ID"
              variant="outlined"
              fullWidth
              disabled
              value={id}
            />
            <TextField
              label="Name"
              variant="outlined"
              fullWidth
              value={name}
              onChange={(e) => handleChangeName(e)}
            />
          </Box>

          {/* Description */}
          <Box marginBottom="15px">
            <TextField
              label="Description"
              variant="outlined"
              fullWidth
              value={description}
              onChange={(e) => handleChangeDesc(e)}
            />
          </Box>

          {/* Action */}
          <Box display="flex" justifyContent="flex-end" gap="15px">
            <Button
              color="success"
              variant="contained"
              onClick={() => handleUpdateType()}
            >
              Update
            </Button>
            <Button
              color="error"
              variant="contained"
              onClick={() => handleClose()}
            >
              Cancel
            </Button>
          </Box>
        </Box>
      </Modal>
    </>
  );
};

export default ModalDetailType;