import { Box, Typography, Modal, Button } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { handleCloseDeleteModalCustomerAction } from '../../store/actions/admin/modalAdmin';
import { fetchApiDeleteCustomerByIdAction } from '../../store/actions/admin/apiRequestAdmin';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

const ModalDeleteCustomer = () => {
  const dispatch = useDispatch();
  const { deleteModalCustomer } = useSelector(reduxData => reduxData.adminReducer);
  const { open, id } = deleteModalCustomer;

  const handleClose = () => {
    dispatch(handleCloseDeleteModalCustomerAction());
  }

  const handleDeleteCustomer = () => {
    dispatch(fetchApiDeleteCustomerByIdAction(id));
  }

  return (
    <>
      <Modal
        open={open}
        onClose={handleClose}
      >
        <Box sx={style}>
          <Typography variant="h4" component="h2">
            Delete Customer
          </Typography>
          <Typography sx={{ mt: 2, mb: 2 }}>
            Are you sure you want to delete this customer?
          </Typography>

          <Box display="flex" justifyContent="flex-end" gap="15px">
            <Button
              variant="contained"
              color="error"
              onClick={() => handleDeleteCustomer()}
            >
              Delete
            </Button>
            <Button
              variant="contained"
              color="inherit"
              onClick={() => handleClose()}
            >
              Cancel
            </Button>
          </Box>
        </Box>
      </Modal>
    </>
  );
};

export default ModalDeleteCustomer;