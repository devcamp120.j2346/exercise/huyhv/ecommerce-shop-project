import { Typography, Modal, Button, Box } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { handleCloseDeleteModalAction } from '../../store/actions/admin/modalAdmin';
import { fetchApiDeleteProductByIdAction } from '../../store/actions/admin/apiRequestAdmin';

const ModalDeleteProduct = () => {
  const dispatch = useDispatch();
  const { deleteModal } = useSelector(reduxData => reduxData.adminReducer);
  const { deleteProduct } = useSelector(reduxData => reduxData.adminReducer);

  const { isError, pending, message } = deleteProduct;
  const { open, id } = deleteModal

  const handleClose = () => {
    dispatch(handleCloseDeleteModalAction());
  };

  const handleDeleteProduct = (id) => {
    dispatch(fetchApiDeleteProductByIdAction(id));
  }

  const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
  };

  return (
    <>
      <Modal
        open={open}
        onClose={handleClose}
      >
        <Box sx={style}>
          <Typography variant="h4" component="h2">
            Delete Product
          </Typography>

          <Box margin="20px 0 20px 0">
            <Typography>
              Are you sure you want to delete this product?
            </Typography>
          </Box>
          <Box
            display="flex"
            gap="15px"
            justifyContent="flex-end"
          >
            <Button
              variant="contained"
              color="error"
              onClick={() => handleDeleteProduct(id)}
            >
              Delete
            </Button>
            <Button
              variant="contained"
              color="inherit"
              onClick={() => handleClose()}
            >
              Cancel
            </Button>
          </Box>
        </Box>
      </Modal>
    </>
  );
};

export default ModalDeleteProduct;