import { Box, Typography, Button, Modal, TextField } from "@mui/material";
import {
  handleCloseDetailModalCustomerAction,
  handleOpenChildModalOrderCustomerAction,
} from '../../store/actions/admin/modalAdmin';
import {
  fetchApiOrderByIdAction,
  fetchApiUpdateCustomerByIdAction
} from '../../store/actions/admin/apiRequestAdmin';
import {
  handleChangeFullNameCustomerAction,
  handleChangePhoneCustomerAction,
  handleChangeEmailCustomerAction,
  handleChangeAddressCustomerAction,
  handleChangeCityCustomerAction,
  handleChangeCountryCustomerAction
} from '../../store/actions/admin/changeInput';
import { useDispatch, useSelector } from "react-redux";
import ModalChildOrderCustomer from "./ModalChildOrderCustomer";

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 800,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

const ModalDetailCustomer = () => {
  const dispatch = useDispatch();
  const { detailModalCustomer } = useSelector(reduxData => reduxData.adminReducer);

  const { open, id, fullName, phone, email, address, city, country, orders } = detailModalCustomer;

  const handleClose = () => {
    dispatch(handleCloseDetailModalCustomerAction());
  }

  // Open Child Modal
  const handleOpenChildModalOrder = () => {

    orders.map(order => {
      dispatch(fetchApiOrderByIdAction(order));
    })

    dispatch(handleOpenChildModalOrderCustomerAction());
  }

  // Change info Customer
  const handleChangeFullName = (e) => {
    dispatch(handleChangeFullNameCustomerAction(e.target.value));
  }
  const handleChangePhone = (e) => {
    dispatch(handleChangePhoneCustomerAction(e.target.value));
  }
  const handleChangeAddress = (e) => {
    dispatch(handleChangeAddressCustomerAction(e.target.value));
  }
  const handleChangeEmail = (e) => {
    dispatch(handleChangeEmailCustomerAction(e.target.value));
  }
  const handleChangeCity = (e) => {
    dispatch(handleChangeCityCustomerAction(e.target.value));
  }
  const handleChangeCountry = (e) => {
    dispatch(handleChangeCountryCustomerAction(e.target.value));
  }

  // Update info User
  const newInfoUser = {
    fullName: fullName.trim(),
    phone: phone.trim(),
    email: email.trim(),
    address: address.trim(),
    city: city.trim(),
    country: country.trim(),
  }

  const handleUpdateInfo = () => {
    dispatch(fetchApiUpdateCustomerByIdAction(id, newInfoUser));
  }

  return (
    <>
      <Modal
        open={open}
        onClose={handleClose}
      >
        <Box sx={style}>
          <Typography variant="h4" component="h2" sx={{ mb: 2 }}>
            Detail Customer
          </Typography>

          {/* ID & Full Name */}
          <Box display="flex" gap="15px" marginBottom="15px">
            <TextField
              variant="outlined"
              label="ID"
              fullWidth
              value={id}
              disabled
            />
            <TextField
              variant="outlined"
              label="Full Name"
              fullWidth
              value={fullName}
              onChange={(e) => handleChangeFullName(e)}
            />
          </Box>

          {/* Phone & Email */}
          <Box display="flex" gap="15px" marginBottom="15px">
            <TextField
              variant="outlined"
              label="Phone"
              fullWidth
              value={phone}
              onChange={(e) => handleChangePhone(e)}
            />
            <TextField
              variant="outlined"
              label="Email"
              fullWidth
              value={email}
              onChange={(e) => handleChangeEmail(e)}
            />
          </Box>

          {/* Address */}
          <Box marginBottom="15px">
            <TextField
              variant="outlined"
              label="Address"
              fullWidth
              value={address}
              onChange={(e) => handleChangeAddress(e)}
            />
          </Box>

          {/* City & Country */}
          <Box display="flex" gap="15px" marginBottom="15px">
            <TextField
              variant="outlined"
              label="City"
              fullWidth
              value={city}
              onChange={(e) => handleChangeCity(e)}
            />
            <TextField
              variant="outlined"
              label="Country"
              fullWidth
              value={country}
              onChange={(e) => handleChangeCountry(e)}
            />
          </Box>

          {/* Actions */}
          <Box display="flex" gap="15px" justifyContent="flex-end">
            <Button
              variant="contained"
              color="success"
              onClick={() => handleUpdateInfo()}
            >
              Update
            </Button>
            <Button
              variant="contained"
              color="info"
              onClick={() => handleOpenChildModalOrder()}
            >
              View Order
            </Button>
            <Button
              variant="contained"
              color="error"
              onClick={() => handleClose()}
            >
              Cancel
            </Button>

          </Box>

          {/* <ChildModal /> */}
          <ModalChildOrderCustomer />
        </Box>
      </Modal>
    </>
  );
};

export default ModalDetailCustomer;