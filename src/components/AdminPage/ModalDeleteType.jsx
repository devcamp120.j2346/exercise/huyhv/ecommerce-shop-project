import { Box, Button, Typography, Modal } from "@mui/material";
import { handleCloseDeleteModalTypeAction } from '../../store/actions/admin/modalAdmin';
import { useDispatch, useSelector } from "react-redux";
import { fetchApiDeleteProductTypeByIdAction } from '../../store/actions/admin/apiRequestAdmin';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

const ModalDeleteType = () => {
  const dispatch = useDispatch();
  const { deleteModalType } = useSelector(reduxData => reduxData.adminReducer);

  const { open, id } = deleteModalType;

  const handleClose = () => {
    dispatch(handleCloseDeleteModalTypeAction());
  }

  const handleDeleteType = () => {
    // console.log('delete type with id: ', id);
    dispatch(fetchApiDeleteProductTypeByIdAction(id));
  }

  return (
    <>
      <Modal
        open={open}
        onClose={handleClose}
      >
        <Box sx={style}>
          <Typography variant="h4" component="h2">
            Delete Product Type
          </Typography>
          <Typography sx={{ mt: 2, mb: 2 }}>
            Are you sure you want to delete this Product Type?
          </Typography>

          <Box
            display="flex"
            justifyContent="flex-end"
            gap="15px"
          >
            <Button
              color="error"
              variant="contained"
              onClick={() => handleDeleteType()}
            >
              Delete
            </Button>
            <Button
              color="inherit"
              variant="contained"
              onClick={() => handleClose()}
            >
              Cancel
            </Button>
          </Box>
        </Box>
      </Modal>
    </>
  );
};

export default ModalDeleteType;