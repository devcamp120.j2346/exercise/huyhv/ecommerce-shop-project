import { useDispatch, useSelector } from "react-redux";
import { Modal, Box, Typography, TextField, Button } from "@mui/material";
import { handleCloseChildModalOrderCustomerAction } from '../../store/actions/admin/modalAdmin';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 800,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

const ModalChildOrderCustomer = () => {
  const dispatch = useDispatch();
  const { detailModalCustomer } = useSelector(reduxData => reduxData.adminReducer);
  const { childModalOrder, orders } = detailModalCustomer;
  const { open, listOrderCustomer, listOrderDetail, listOrderProduct } = childModalOrder;

  const handleClose = () => {
    dispatch(handleCloseChildModalOrderCustomerAction())
  };

  return (
    <>
      <Modal
        open={open}
        onClose={handleClose}
      >
        <Box sx={{ ...style, width: 500 }}>
          <Typography variant="h4" sx={{ mb: 5 }}>List Order: {orders.length}</Typography>

          {/* Order Date */}
          {listOrderCustomer.map((order, index) => (
            <Box key={order.data._id}>
              <Typography sx={{ mb: 2, mt: 2 }}>Item: {index + 1}</Typography>
              <TextField
                variant="outlined"
                label="Order Date"
                fullWidth
                sx={{ mb: "15px" }}
                value={order.data.orderDate}
              />

              {/* Note */}
              <TextField
                variant="outlined"
                label="Note"
                fullWidth
                sx={{ mb: "15px" }}
                value={order.data.note}
              />

              {/* Cost */}
              <TextField
                variant="outlined"
                label="Total Cost"
                fullWidth
                sx={{ mb: "15px" }}
                value={order.data.cost}
              />

              {listOrderDetail.map((orderCustomer, index) => (
                <Box
                  key={index}
                  display="flex"
                  gap="15px"
                  marginBottom="15px"
                >
                  <TextField
                    variant="outlined"
                    label="Product"
                    fullWidth
                    value={listOrderProduct[index]?.data.name || ''}
                  />
                  <TextField
                    variant="outlined"
                    label="Quantity"
                    fullWidth
                    value={orderCustomer.data.quantity}
                  />
                </Box>
              ))}
            </Box>
          ))}
          <Button
            onClick={handleClose}
            variant="contained"
            color="inherit"
          >
            Close Child Modal
          </Button>
        </Box>
      </Modal>
    </>
  );
};

export default ModalChildOrderCustomer;