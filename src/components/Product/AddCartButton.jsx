import { handleAddCartAction } from '../../store/actions/addCart';
import { useDispatch } from 'react-redux';

const AddCartButton = ({ product }) => {
  const dispatch = useDispatch();

  const handleAddCart = () => {
    dispatch(handleAddCartAction(product));
  }

  return (
    <>
      <i
        className="fa-solid fa-bag-shopping bg-[#DAE5DA] p-[13px] rounded-full text-[#002602] cursor-pointer text-[24px] hover:bg-[#00B207] hover:text-white duration-200"
        onClick={() => handleAddCart()}
      ></i>
    </>
  );
};

export default AddCartButton;