import { useState } from "react";
import { FaStar } from 'react-icons/fa'

const Rating = () => {
  const [rating, setRating] = useState(null);
  const [hover, setHover] = useState(null);

  return (
    <div className="flex items-center">
      {[...Array(5)].map((star, index) => {

        const currentRating = index + 1;

        return (
          <label key={index}>
            <input
              type="radio"
              name="rating"
              className="hidden"
              value={currentRating}
              onClick={() => setRating(currentRating)}
            />
            <FaStar
              size={18}
              className="cursor-pointer"
              color={currentRating <= (hover || rating) ? "#ffc107" : "#e4e5e9"}
              onMouseEnter={() => setHover(currentRating)}
              onMouseLeave={() => setHover(null)}
            />
          </label>
        )
      })}
      {/* <p>Your Rating is: {rating}</p> */}
    </div>
  );
};

export default Rating;