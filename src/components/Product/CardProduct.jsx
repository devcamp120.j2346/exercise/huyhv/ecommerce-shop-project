import AddCartButton from './AddCartButton';
import Rating from './Rating';
import { useNavigate } from 'react-router-dom';

const CardProduct = ({ id, name, imageUrl, buyPrice, promotionPrice, amount, product }) => {
  const navigate = useNavigate();

  const handleViewDetail = () => {
    navigate(`/shop/${id}`);
  }

  return (
    <div
      className='bg-white rounded-lg overflow-hidden border hover:border hover:border-[#00B207] hover:shadow-xl duration-200'
    >
      {/* Image Product */}
      <div className='relative cursor-pointer' onClick={() => handleViewDetail()}>

        {/* Out Of Stock */}
        {amount === 0 ?
          <div className='bg-black absolute py-[3px] px-2 rounded m-3'>
            <p className='text-white text-[14px] leading-[21px]'>Out of Stock</p>
          </div>
          :
          <></>}

        <img src={imageUrl} alt='product-image' className='p-[5px] w-full h-full lg:h-[300px] object-cover' />
      </div>

      {/* Info Product */}
      <div className='flex items-center justify-between py-4 px-5'>
        <div>
          <h4
            className='text-[#2B572E] leading-6 mb-[2px] cursor-pointer'
            onClick={() => handleViewDetail()}
          >
            {name}
          </h4>
          <p className='mb-[11px] flex items-center gap-1'>
            <span className='text-[#002603] text-[18px] font-medium leading-[27px]'>${promotionPrice}</span>
            <span className='text-[#7A997C] line-through leading-6'>${buyPrice}</span>
          </p>
          <Rating />
        </div>
        <AddCartButton product={product} />
      </div>
    </div>
  );
};

export default CardProduct;