import Pagination from '@mui/material/Pagination';
import CardProduct from '../Product/CardProduct';
import FilterCategory from './FilterCategory';
import FilterPrice from './FilterPrice';
import DiscountOrder from './DiscountOrder';

import { useDispatch, useSelector } from 'react-redux';
import { fetchApiProductAction } from '../../store/actions/apiRequest';
import { useEffect } from 'react';
import { handleChangePaginationAction } from '../../store/actions/pagination.action';

const Shop = () => {
  const dispatch = useDispatch();
  const { product } = useSelector(reduxData => reduxData.productReducer);
  const { currentPage, limit } = useSelector(reduxData => reduxData.paginationReducer);
  const { listProduct, totalProduct } = product;

  const handleChangePagination = (event, newValue) => {
    dispatch(handleChangePaginationAction(newValue));
  }

  useEffect(() => {
    dispatch(fetchApiProductAction(0, currentPage, limit));
  }, [currentPage])

  return (
    <div className="max-w-screen-xl mx-auto px-4 mb-20">
      <div className='flex gap-6'>
        {/* Filter */}
        <div className='flex-1'>
          <FilterCategory />
          <hr />
          <FilterPrice />
          <DiscountOrder />
        </div>

        {/* Product & Pagination */}
        <div className='flex-[3]'>
          {/* List Product */}
          <div className='grid grid-cols-1 md:grid-cols-2 xl:grid-cols-3 gap-6 mb-10'>
            {listProduct && listProduct?.map(item => (
              <CardProduct
                key={item._id}
                id={item._id}
                name={item.name}
                imageUrl={item.imageUrl}
                buyPrice={item.buyPrice}
                promotionPrice={item.promotionPrice}
                amount={item.amount}
                product={item}
              />
            ))}
          </div>

          {/* Pagination */}
          <div className='flex justify-center'>
            <Pagination
              count={Math.ceil(totalProduct / limit) || 0}
              page={currentPage}
              onChange={handleChangePagination}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Shop;