import {
  List,
  ListItemButton,
  ListItemText,
  Collapse,
  Slider
} from '@mui/material';
import { ExpandLess, ExpandMore } from '@mui/icons-material';
import { useState } from 'react';

const FilterPrice = () => {
  const [value, setValue] = useState([20, 100]);
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  // Select MUI
  const [openPrice, setOpenPrice] = useState(true);
  const handleOpenPrice = () => {
    setOpenPrice(!openPrice)
  }

  return (
    <List
      sx={{ width: '100%', bgcolor: 'background.paper' }}
    >
      <ListItemButton onClick={handleOpenPrice}>
        <ListItemText primary="Price" />
        {openPrice ? <ExpandLess /> : <ExpandMore />}
      </ListItemButton>
      <Collapse in={openPrice} timeout="auto" unmountOnExit>
        <List component="div" disablePadding>
          <Slider
            value={value}
            onChange={handleChange}
            valueLabelDisplay="auto"
            color='success'
            disableSwap
            min={0}
            max={200}
          />
          <p className='text-[14px] leading-[21px]'>
            <span className='text-gray-700'>Price: </span>
            <span className='font-medium'>{value[0]} - {value[1]}</span>
          </p>
        </List>
      </Collapse>
    </List>
  );
};

export default FilterPrice;