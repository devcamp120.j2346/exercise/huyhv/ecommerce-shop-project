import {
  List,
  ListItemButton,
  ListItemText,
  Collapse,
  RadioGroup,
  FormControlLabel,
  Radio
} from '@mui/material';
import { ExpandLess, ExpandMore } from '@mui/icons-material';
import { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchApiProductTypeAction } from '../../store/actions/apiRequest';

const FilterCategory = () => {
  const dispatch = useDispatch();
  const [openCategory, setOpenCategory] = useState(true);

  const { productType } = useSelector(reduxData => reduxData.productReducer)
  const { listProductType } = productType;

  const handleOpenCategory = () => {
    setOpenCategory(!openCategory);
  };

  useEffect(() => {
    dispatch(fetchApiProductTypeAction())
  }, [])

  return (
    <List
      sx={{ width: '100%', bgcolor: 'background.paper' }}
    >
      <ListItemButton onClick={handleOpenCategory}>
        <ListItemText primary="All Categories" />
        {openCategory ? <ExpandLess /> : <ExpandMore />}
      </ListItemButton>
      <Collapse in={openCategory} timeout="auto" unmountOnExit>
        <List component="div" disablePadding>
          <RadioGroup
            sx={{ pl: 1 }}
            name="category"
          >
            {listProductType && listProductType.data?.length > 0 &&
              listProductType.data?.map(item => (
                <FormControlLabel
                  key={item._id}
                  value={item.name}
                  control={<Radio />}
                  label={item.name}
                />
              ))}
          </RadioGroup>
        </List>
      </Collapse>
    </List>
  );
};

export default FilterCategory;