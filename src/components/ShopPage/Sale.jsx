const Sale = () => {
  return (
    <div className='max-w-screen-xl mx-auto px-4 my-10'>
      <div className='py-[68px] px-4 md:px-[60px] rounded-[10px] bg-[url("/src/assets/images/breadcrumb/breadcrumb-1.jpg")] bg-no-repeat bg-cover bg-bottom'>
        <p className='text-white text-[14px] font-medium uppercase leading-[14px] tracking-[0.42px] mb-3'>BEST DEALS</p>
        <h2 className='text-white text-[30px] md:text-[40px] font-semibold leading-[48px] mb-5'>Sale of the Month</h2>
        <button className='text-white text-[14px] font-semibold leading-4 bg-[#00B207] py-[14px] px-8 rounded-full'>Shop Now</button>
      </div>
    </div>
  );
};

export default Sale;