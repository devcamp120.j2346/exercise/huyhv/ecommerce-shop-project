import { useState } from 'react';
import { customerFeedback } from '../../store/customerFeedback';
import { Box, Tab } from '@mui/material';
import { TabContext, TabList, TabPanel } from '@mui/lab';

import CommentCustomer from './CommentCustomer';
import AddInformation from './AddInformation';
import DescProduct from './DescProduct';

const DescriptionProduct = () => {
  const [value, setValue] = useState('1');

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  return (
    <div className='max-w-screen-xl mx-auto px-4'>
      <Box sx={{ width: '100%', typography: 'body1' }}>
        <TabContext value={value}>
          <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
            <TabList onChange={handleChange} centered={true}>
              <Tab label="Descriptions" value="1" />
              <Tab label="Additional Information" value="2" />
              <Tab label="Customer Feedback" value="3" />
            </TabList>
          </Box>
          <TabPanel value="1">
            <DescProduct />
          </TabPanel>

          <TabPanel value="2">
            <AddInformation />
          </TabPanel>

          <TabPanel value="3">
            {customerFeedback.map(customer => (
              <CommentCustomer
                key={customer.id}
                avatar={customer.avatar}
                name={customer.name}
                time={customer.time}
                comment={customer.comment}
              />
            ))}
          </TabPanel>
        </TabContext>
      </Box>
    </div>
  );
};

export default DescriptionProduct;