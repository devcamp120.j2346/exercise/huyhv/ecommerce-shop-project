
const AddInformation = () => {
  return (
    <div className='flex'>
      {/* Column 1 */}
      <div className='flex-1 flex flex-col gap-3'>
        <p className='text-gray-900 text-[14px] font-normal font-[Poppins]'>Weight:</p>
        <p className='text-gray-900 text-[14px] font-normal font-[Poppins]'>Color:</p>
        <p className='text-gray-900 text-[14px] font-normal font-[Poppins]'>Type:</p>
        <p className='text-gray-900 text-[14px] font-normal font-[Poppins]'>Category:</p>
        <p className='text-gray-900 text-[14px] font-normal font-[Poppins]'>Stock Status:</p>
      </div>

      {/* Column 2 */}
      <div className='flex-[7] flex flex-col gap-3'>
        <p className='text-gray-500 text-[14px] font-normal font-[Poppins]'>03</p>
        <p className='text-gray-500 text-[14px] font-normal font-[Poppins]'>Green</p>
        <p className='text-gray-500 text-[14px] font-normal font-[Poppins]'>Organic</p>
        <p className='text-gray-500 text-[14px] font-normal font-[Poppins]'>Vegetables</p>
        <p className='text-gray-500 text-[14px] font-normal font-[Poppins]'>Available (5,413)</p>
      </div>
    </div>
  );
};

export default AddInformation;