import { FaCircleCheck } from "react-icons/fa6";

const DescProduct = () => {
  return (
    <>
      <p className='text-gray-500 text-[14px] font-normal font-[Poppins] mb-5'>
        Sed commodo aliquam dui ac porta. Fusce ipsum felis, imperdiet at posuere ac, viverra at mauris. Maecenas tincidunt ligula a sem vestibulum pharetra. Maecenas auctor tortor lacus, nec laoreet nisi porttitor vel. Etiam tincidunt metus vel dui interdum sollicitudin. Mauris sem ante, vestibulum nec orci vitae, aliquam mollis lacus. Sed et condimentum arcu, id molestie tellus. Nulla facilisi. Nam scelerisque vitae justo a convallis. Morbi urna ipsum, placerat quis commodo quis, egestas elementum leo. Donec convallis mollis enim. Aliquam id mi quam. Phasellus nec fringilla elit.
        <br />
        <br />
        Nulla mauris tellus, feugiat quis pharetra sed, gravida ac dui. Sed iaculis, metus faucibus elementum tincidunt, turpis mi viverra velit, pellentesque tristique neque mi eget nulla. Proin luctus elementum neque et pharetra.
      </p>
      <div className='flex flex-col gap-[14px] mb-5'>
        <div className='flex items-center gap-2'>
          <FaCircleCheck className='text-[#00B207]' />
          <p className='text-gray-500 text-[14px] font-normal font-[Poppins]'>100 g of fresh leaves provides.</p>
        </div>
        <div className='flex items-center gap-2'>
          <FaCircleCheck className='text-[#00B207]' />
          <p className='text-gray-500 text-[14px] font-normal font-[Poppins]'>Aliquam ac est at augue volutpat elementum.</p>
        </div>
        <div className='flex items-center gap-2'>
          <FaCircleCheck className='text-[#00B207]' />
          <p className='text-gray-500 text-[14px] font-normal font-[Poppins]'>Quisque nec enim eget sapien molestie.</p>
        </div>
        <div className='flex items-center gap-2'>
          <FaCircleCheck className='text-[#00B207]' />
          <p className='text-gray-500 text-[14px] font-normal font-[Poppins]'>Proin convallis odio volutpat finibus posuere.</p>
        </div>
      </div>
      <p className='text-gray-500 text-[14px] font-normal font-[Poppins]'>Cras et diam maximus, accumsan sapien et, sollicitudin velit. Nulla blandit eros non turpis lobortis iaculis at ut massa. </p>
    </>
  );
};

export default DescProduct;