import star from '../../assets/icons/star-icon.svg';

const CommentCustomer = ({ avatar, name, time, comment }) => {
  return (
    <>
      <div className='my-5'>
        {/* Info User */}
        <div className='flex justify-between items-center'>

          {/* Avatar */}
          <div className='flex items-center gap-3'>
            <img src={avatar} alt='user-image' className='w-10 h-10 object-cover rounded-full' />
            <div>
              <p className='text-gray-900 text-[14px] font-medium font-[Poppins]'>{name}</p>
              <div className='flex items-center'>
                <img src={star} />
                <img src={star} />
                <img src={star} />
                <img src={star} />
                <img src={star} />
              </div>
            </div>

          </div>
          <p className='text-gray-400 text-[14px] font-[Poppins]'>{time}</p>
        </div>

        {/* Comment */}
        <p className='text-gray-500 text-[14px] font-[Poppins] mt-3'>{comment}</p>
      </div>
      <hr />
    </>

  );
};

export default CommentCustomer;