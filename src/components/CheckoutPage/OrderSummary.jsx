import {
  Radio,
  RadioGroup,
  FormControlLabel,
  FormControl,
} from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';
import { handleSubmitOrderAction } from '../../store/actions/apiRequest'

const OrderSummary = () => {
  const dispatch = useDispatch();
  const { listCart, subTotal, customer } = useSelector(reduxData => reduxData.productReducer);
  const { firstName, lastName, company, address, country, city, email, phone, note } = customer;

  const newCustomer = {
    fullName: lastName + firstName,
    phone,
    email,
    address,
    city,
    country,
  }

  const handleSubmitOrder = () => {
    dispatch(handleSubmitOrderAction(newCustomer, note, subTotal, listCart))
  }

  return (
    <div className="flex-1 border rounded-lg p-6 h-fit">
      <h2 className="mb-3 text-gray-900 text-[20px] font-medium leading-[30px]">Order Summary</h2>

      {/* List Cart */}
      {listCart && listCart.map(item => (
        <div key={item._id} className='flex flex-col sm:flex-row sm:items-center justify-between mb-3'>
          <div className='flex gap-[6px] items-center'>
            <img src={item.imageUrl} className='w-[60px] h-[60px] object-cover' />
            <p>{item.name}</p>
            <p>x{item.quantity}</p>
          </div>
          <p className='text-right'>${item.promotionPrice * item.quantity}</p>
        </div>
      ))}

      {/* SubTotal */}
      <div className='flex items-center justify-between py-3 border-b'>
        <p className='text-gray-700 text-[14px]'>Subtotal:</p>
        <p className='text-gray-900 text-[14px] font-medium'>${subTotal}</p>
      </div>

      <div className='flex items-center justify-between py-3 border-b'>
        <p className='text-gray-700 text-[14px]'>Shipping:</p>
        <p className='text-gray-900 text-[14px] font-medium'>Free</p>
      </div>

      <div className='flex items-center justify-between pt-3 mb-6'>
        <p className='text-gray-700 text-[14px]'>Total:</p>
        <p className='text-gray-900 text-[18px] font-semibold'>${subTotal}</p>
      </div>

      {/* Payment */}
      <h2 className='text-gray-900 text-[20px] font-medium leading-[30px] mb-4'>Payment Method</h2>

      <div className='mb-6'>
        <FormControl>
          <RadioGroup defaultValue="cash">
            <FormControlLabel
              value="cash"
              control={<Radio color='success' />}
              label="Cash on Delivery"
            />
            <FormControlLabel
              value="paypal"
              control={<Radio color='success' />}
              label="Paypal"
            />
          </RadioGroup>
        </FormControl>
      </div>

      <button
        className='text-white font-semibold py-4 px-10 bg-[#00B207] rounded-full w-full'
        onClick={() => handleSubmitOrder()}
      >
        Place Order
      </button>
    </div>
  );
};

export default OrderSummary;