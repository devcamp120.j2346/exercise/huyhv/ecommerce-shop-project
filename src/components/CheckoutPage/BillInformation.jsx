import {
  handleChangeAddressAction,
  handleChangeCityAction,
  handleChangeCompanyNameAction,
  handleChangeCountryAction,
  handleChangeEmailAction,
  handleChangeFirstNameAction,
  handleChangeLastNameAction,
  handleChangeNoteAction,
  handleChangePhoneAction
} from '../../store/actions/changeInformation';
import { useDispatch, useSelector } from 'react-redux';

const BillInformation = () => {
  const dispatch = useDispatch();
  const { customer } = useSelector(reduxData => reduxData.productReducer);
  const { firstName, lastName, company, address, country, city, email, phone, note } = customer;

  const handleChangeFirstName = (e) => {
    dispatch(handleChangeFirstNameAction(e.target.value));
  }
  const handleChangeLastName = (e) => {
    dispatch(handleChangeLastNameAction(e.target.value));
  }
  const handleChangeCompanyName = (e) => {
    dispatch(handleChangeCompanyNameAction(e.target.value));
  }
  const handleChangeAddress = (e) => {
    dispatch(handleChangeAddressAction(e.target.value));
  }
  const handleChangeCountry = (e) => {
    dispatch(handleChangeCountryAction(e.target.value));
  }
  const handleChangeCity = (e) => {
    dispatch(handleChangeCityAction(e.target.value));
  }
  const handleChangeEmail = (e) => {
    dispatch(handleChangeEmailAction(e.target.value));
  }
  const handleChangePhone = (e) => {
    dispatch(handleChangePhoneAction(e.target.value));
  }
  const handleChangeNote = (e) => {
    dispatch(handleChangeNoteAction(e.target.value));
  }


  return (
    <div className="flex-[2]">
      <form>
        <h2 className="text-gray-900 text-[24px] font-medium leading-9 mb-5">Billing Information</h2>

        <div className="grid grid-cols-1 lg:grid-cols-3 gap-4 mb-4">
          {/* First name */}
          <div className="flex flex-col gap-2">
            <label className="text-gray-900 text-[14px]">First name</label>
            <input
              type="text"
              placeholder="Your first name"
              className="px-4 py-[14px] border rounded-md w-full outline-none"
              value={firstName}
              onChange={(e) => handleChangeFirstName(e)}
            />
          </div>

          {/* Last name */}
          <div className="flex flex-col gap-2">
            <label className="text-gray-900 text-[14px]">Last name</label>
            <input
              type="text"
              placeholder="Your last name"
              className="px-4 py-[14px] border rounded-md w-full outline-none"
              value={lastName}
              onChange={(e) => handleChangeLastName(e)}
            />
          </div>

          {/* Company name */}
          <div className="flex flex-col gap-2">
            <label className="text-gray-900 text-[14px]">
              Company Name
              <span className="text-gray-500"> (optional)</span>
            </label>
            <input
              type="text"
              placeholder="Company name"
              className="px-4 py-[14px] border rounded-md w-full outline-none"
              value={company}
              onChange={(e) => handleChangeCompanyName(e)}
            />
          </div>
        </div>

        <div className="flex flex-col gap-2 mb-4">
          <label className="text-gray-900 text-[14px]">Street Address</label>
          <input
            type="text"
            placeholder="Street Address"
            className="px-4 py-[14px] border rounded-md w-full outline-none"
            value={address}
            onChange={(e) => handleChangeAddress(e)}
          />
        </div>

        <div className="grid grid-cols-1 md:grid-cols-2 gap-4 mb-4">
          {/* Country */}
          <div className="flex flex-col gap-2">
            <label className="text-gray-900 text-[14px]">Country / Region</label>
            <input
              type="text"
              placeholder="Country"
              className="px-4 py-[14px] border rounded-md w-full outline-none"
              value={country}
              onChange={(e) => handleChangeCountry(e)}
            />
          </div>

          {/* City */}
          <div className="flex flex-col gap-2">
            <label className="text-gray-900 text-[14px]">City</label>
            <input
              type="text"
              placeholder="City"
              className="px-4 py-[14px] border rounded-md w-full outline-none"
              value={city}
              onChange={(e) => handleChangeCity(e)}
            />
          </div>
        </div>

        <div className="grid grid-cols-1 md:grid-cols-2 gap-4">
          <div className="flex flex-col gap-2">
            <label className="text-gray-900 text-[14px]">Email</label>
            <input
              type="text"
              placeholder="Email Address"
              className="px-4 py-[14px] border rounded-md w-full outline-none"
              value={email}
              onChange={(e) => handleChangeEmail(e)}
            />
          </div>

          <div className="flex flex-col gap-2">
            <label className="text-gray-900 text-[14px]">Phone</label>
            <input
              type="text"
              placeholder="Phone number"
              className="px-4 py-[14px] border rounded-md w-full outline-none"
              value={phone}
              onChange={(e) => handleChangePhone(e)}
            />
          </div>
        </div>

        <hr className="my-8" />

        <h2 className="text-gray-900 text-[24px] font-medium leading-9 mb-5">Additional Info</h2>
        <div className="flex flex-col gap-2">
          <label className="text-gray-900 text-[14px]">Order Notes (Optional)</label>
          <textarea
            placeholder="Notes about your order, e.g. special notes for delivery"
            className="px-4 py-[14px] border rounded-md w-full outline-none h-[100px]"
            value={note}
            onChange={(e) => handleChangeNote(e)}
          ></textarea>
        </div>
      </form>
    </div>
  );
};

export default BillInformation;