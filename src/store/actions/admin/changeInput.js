import {
  // Product
  HANDLE_CHANGE_AMOUNT_PRODUCT,
  HANDLE_CHANGE_BUY_PRICE_PRODUCT,
  HANDLE_CHANGE_DESC_PRODUCT,
  HANDLE_CHANGE_IMAGE_URL_PRODUCT,
  HANDLE_CHANGE_NAME_PRODUCT,
  HANDLE_CHANGE_PROMOTION_PRICE_PRODUCT,
  HANDLE_CHANGE_TYPE_PRODUCT,

  // Product Type
  HANDLE_CHANGE_NAME_TYPE,
  HANDLE_CHANGE_DESC_TYPE,
} from '../../constants/admin/productAdmin';
import {
  HANDLE_CHANGE_ADDRESS_CUSTOMER,
  HANDLE_CHANGE_CITY_CUSTOMER,
  HANDLE_CHANGE_FULLNAME_CUSTOMER,
  HANDLE_CHANGE_COUNTRY_CUSTOMER,
  HANDLE_CHANGE_EMAIL_CUSTOMER,
  HANDLE_CHANGE_PHONE_CUSTOMER
} from '../../constants/admin/customerAdmin';

// Handle Change Input Product
export const handleChangeNameAction = (inputName) => {
  return {
    type: HANDLE_CHANGE_NAME_PRODUCT,
    payload: inputName
  }
}
export const handleChangeDescAction = (inputDesc) => {
  return {
    type: HANDLE_CHANGE_DESC_PRODUCT,
    payload: inputDesc
  }
}
export const handleChangeTypeAction = (inputType) => {
  return {
    type: HANDLE_CHANGE_TYPE_PRODUCT,
    payload: inputType
  }
}
export const handleChangeImageAction = (inputImage) => {
  return {
    type: HANDLE_CHANGE_IMAGE_URL_PRODUCT,
    payload: inputImage
  }
}
export const handleChangeBuyPriceAction = (inputBuyPrice) => {
  return {
    type: HANDLE_CHANGE_BUY_PRICE_PRODUCT,
    payload: inputBuyPrice
  }
}
export const handleChangePromotionPriceAction = (inputPromotionPrice) => {
  return {
    type: HANDLE_CHANGE_PROMOTION_PRICE_PRODUCT,
    payload: inputPromotionPrice
  }
}
export const handleChangeAmountAction = (inputAmount) => {
  return {
    type: HANDLE_CHANGE_AMOUNT_PRODUCT,
    payload: inputAmount
  }
}

// Handle Change Input Product Type
export const handleChangeNameTypeAction = (inputName) => {
  return {
    type: HANDLE_CHANGE_NAME_TYPE,
    payload: inputName
  }
}
export const handleChangeDescTypeAction = (inputDesc) => {
  return {
    type: HANDLE_CHANGE_DESC_TYPE,
    payload: inputDesc
  }
}

// Handle Change Input Customer
export const handleChangeFullNameCustomerAction = (inputFullName) => {
  return {
    type: HANDLE_CHANGE_FULLNAME_CUSTOMER,
    payload: inputFullName
  }
}
export const handleChangePhoneCustomerAction = (inputPhone) => {
  return {
    type: HANDLE_CHANGE_PHONE_CUSTOMER,
    payload: inputPhone
  }
}
export const handleChangeEmailCustomerAction = (inputEmail) => {
  return {
    type: HANDLE_CHANGE_EMAIL_CUSTOMER,
    payload: inputEmail
  }
}
export const handleChangeAddressCustomerAction = (inputAddress) => {
  return {
    type: HANDLE_CHANGE_ADDRESS_CUSTOMER,
    payload: inputAddress
  }
}
export const handleChangeCityCustomerAction = (inputCity) => {
  return {
    type: HANDLE_CHANGE_CITY_CUSTOMER,
    payload: inputCity
  }
}
export const handleChangeCountryCustomerAction = (inputCountry) => {
  return {
    type: HANDLE_CHANGE_COUNTRY_CUSTOMER,
    payload: inputCountry
  }
}