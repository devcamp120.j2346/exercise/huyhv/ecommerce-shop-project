import axios from 'axios';
import {
  // Fetch List Product
  FETCH_PRODUCTS_ERROR,
  FETCH_PRODUCTS_PENDING,
  FETCH_PRODUCTS_SUCCESS,

  // Fetch Product by ID
  FETCH_PRODUCT_BY_ID_ERROR,
  FETCH_PRODUCT_BY_ID_PENDING,
  FETCH_PRODUCT_BY_ID_SUCCESS,

  // Fetch Delete Product by ID
  FETCH_DELETE_PRODUCT_BY_ID_ERROR,
  FETCH_DELETE_PRODUCT_BY_ID_PENDING,
  FETCH_DELETE_PRODUCT_BY_ID_SUCCESS,

  // Fetch Update Product by ID
  FETCH_UPDATE_PRODUCT_BY_ID_ERROR,
  FETCH_UPDATE_PRODUCT_BY_ID_PENDING,
  FETCH_UPDATE_PRODUCT_BY_ID_SUCCESS,

  // Fetch List Product
  FETCH_PRODUCT_TYPE_ERROR,
  FETCH_PRODUCT_TYPE_PENDING,
  FETCH_PRODUCT_TYPE_SUCCESS,

  // Fetch Update Product Type by ID
  FETCH_UPDATE_TYPE_BY_ID_ERROR,
  FETCH_UPDATE_TYPE_BY_ID_PENDING,
  FETCH_UPDATE_TYPE_BY_ID_SUCCESS,

  // Fetch Delete Product Type by ID
  FETCH_DELETE_TYPE_BY_ID_ERROR,
  FETCH_DELETE_TYPE_BY_ID_PENDING,
  FETCH_DELETE_TYPE_BY_ID_SUCCESS,

  // Fetch Order by ID
  FETCH_ORDER_BY_ID_ERROR,
  FETCH_ORDER_BY_ID_PENDING,
  FETCH_ORDER_BY_ID_SUCCESS,

  // Fetch Order by ID
  FETCH_ORDER_DETAIL_BY_ID_ERROR,
  FETCH_ORDER_DETAIL_BY_ID_PENDING,
  FETCH_ORDER_DETAIL_BY_ID_SUCCESS,

  // Create Product
  FETCH_CREATE_PRODUCT_ERROR,
  FETCH_CREATE_PRODUCT_PENDING,
  FETCH_CREATE_PRODUCT_SUCCESS
} from '../../constants/admin/productAdmin';
import {
  // Fetch List Customer
  FETCH_CUSTOMERS_ERROR,
  FETCH_CUSTOMERS_PENDING,
  FETCH_CUSTOMERS_SUCCESS,

  // Fetch Update Customer by ID
  FETCH_UPDATE_CUSTOMER_ERROR,
  FETCH_UPDATE_CUSTOMER_PENDING,
  FETCH_UPDATE_CUSTOMER_SUCCESS,

  // Fetch Delete Customer by ID
  FETCH_DELETE_CUSTOMER_ERROR,
  FETCH_DELETE_CUSTOMER_PENDING,
  FETCH_DELETE_CUSTOMER_SUCCESS
} from '../../constants/admin/customerAdmin';

// Fetch Api List Product
export const fetchApiProductsAction = () => {
  const vAPI_URL_PRODUCTS = 'http://localhost:8080/api/product';

  return async (dispatch) => {
    try {

      await dispatch({
        type: FETCH_PRODUCTS_PENDING
      })

      let response = await axios.get(vAPI_URL_PRODUCTS);

      return dispatch({
        type: FETCH_PRODUCTS_SUCCESS,
        payload: response.data
      })

    } catch (error) {
      return dispatch({
        type: FETCH_PRODUCTS_ERROR,
        error: error.message
      })
    }
  }
}

// Fetch Api Product by ID
export const fetchApiProductByIdAction = (id) => {
  const vAPI_URL_PRODUCT = 'http://localhost:8080/api/product';

  return async (dispatch) => {
    try {
      await dispatch({
        type: FETCH_PRODUCT_BY_ID_PENDING
      })

      let response = await axios.get(vAPI_URL_PRODUCT + '/' + id);

      return dispatch({
        type: FETCH_PRODUCT_BY_ID_SUCCESS,
        payload: response.data
      })
    } catch (error) {
      return dispatch({
        type: FETCH_PRODUCT_BY_ID_ERROR,
        error: error.message
      })
    }
  }
}

// Fetch API List Product Type
export const fetchApiProductTypeAction = () => {
  const vAPI_URL_PRODUCT_TYPE = 'http://localhost:8080/api/productType';

  return async (dispatch) => {
    try {
      await dispatch({
        type: FETCH_PRODUCT_TYPE_PENDING
      })

      let response = await axios.get(vAPI_URL_PRODUCT_TYPE);

      return dispatch({
        type: FETCH_PRODUCT_TYPE_SUCCESS,
        payload: response.data
      })

    } catch (error) {
      return dispatch({
        type: FETCH_PRODUCT_TYPE_ERROR,
        error: error.message
      })
    }
  }
}

// Update Product by ID
export const fetchApiUpdateProductByIdAction = (id, newInfoProduct) => {
  const vAPI_URL_PRODUCTS = 'http://localhost:8080/api/product';

  return async (dispatch) => {
    try {
      await dispatch({
        type: FETCH_UPDATE_PRODUCT_BY_ID_PENDING
      })

      let response = await axios.put(vAPI_URL_PRODUCTS + '/' + id, newInfoProduct);

      await dispatch(fetchApiProductsAction());

      return dispatch({
        type: FETCH_UPDATE_PRODUCT_BY_ID_SUCCESS,
        payload: response.data
      })
    } catch (error) {
      return dispatch({
        type: FETCH_UPDATE_PRODUCT_BY_ID_ERROR,
        error: error.message
      })
    }
  }
}

// Delete Product by ID
export const fetchApiDeleteProductByIdAction = (id) => {
  const vAPI_URL_PRODUCTS = 'http://localhost:8080/api/product';

  return async (dispatch) => {
    try {
      await dispatch({
        type: FETCH_DELETE_PRODUCT_BY_ID_PENDING
      })

      let response = await axios.delete(vAPI_URL_PRODUCTS + '/' + id);

      await dispatch(fetchApiProductsAction());

      return dispatch({
        type: FETCH_DELETE_PRODUCT_BY_ID_SUCCESS,
        payload: response.data
      })
    } catch (error) {
      return dispatch({
        type: FETCH_DELETE_PRODUCT_BY_ID_ERROR,
        error: error.message
      })
    }
  }
}

// Update Product Type by ID
export const fetchApiUpdateProductTypeByIdAction = (id, newInfoProductType) => {
  const vAPI_URL_PRODUCT_TYPE = 'http://localhost:8080/api/productType';

  return async (dispatch) => {
    try {
      await dispatch({
        type: FETCH_UPDATE_TYPE_BY_ID_PENDING
      })

      let response = await axios.put(vAPI_URL_PRODUCT_TYPE + '/' + id, newInfoProductType);

      await dispatch(fetchApiProductTypeAction());

      return dispatch({
        type: FETCH_UPDATE_TYPE_BY_ID_SUCCESS,
        payload: response.data
      })
    } catch (error) {
      return dispatch({
        type: FETCH_UPDATE_TYPE_BY_ID_ERROR,
        error: error.message
      })
    }
  }
}

// Delete Product Type by ID
export const fetchApiDeleteProductTypeByIdAction = (id) => {
  const vAPI_URL_PRODUCT_TYPE = 'http://localhost:8080/api/productType';

  return async (dispatch) => {
    try {
      await dispatch({
        type: FETCH_DELETE_TYPE_BY_ID_PENDING
      })

      let response = await axios.delete(vAPI_URL_PRODUCT_TYPE + '/' + id);

      await dispatch(fetchApiProductTypeAction())

      return dispatch({
        type: FETCH_DELETE_TYPE_BY_ID_SUCCESS,
        payload: response.data
      })

    } catch (error) {
      return dispatch({
        type: FETCH_DELETE_TYPE_BY_ID_ERROR,
        error: error.message
      })
    }
  }
}

// Fetch List Customers
export const fetchApiCustomersAction = () => {
  const vAPI_URL_CUSTOMERS = 'http://localhost:8080/api/customer';

  return async (dispatch) => {
    try {
      await dispatch({
        type: FETCH_CUSTOMERS_PENDING
      })

      let response = await axios.get(vAPI_URL_CUSTOMERS);

      return dispatch({
        type: FETCH_CUSTOMERS_SUCCESS,
        payload: response.data
      })

    } catch (error) {
      return dispatch({
        type: FETCH_CUSTOMERS_ERROR,
        error: error.message
      })
    }
  }
}

// Fetch Order by ID
export const fetchApiOrderByIdAction = (id) => {
  const vAPI_URL_ORDER = 'http://localhost:8080/api/order';

  return async (dispatch) => {
    try {
      await dispatch({
        type: FETCH_ORDER_BY_ID_PENDING
      })

      let response = await axios.get(vAPI_URL_ORDER + '/' + id);

      await response.data.data.orderDetails.map(orderDetailId => {
        dispatch(fetchApiOrderDetailByIdAction(orderDetailId));
      })

      return dispatch({
        type: FETCH_ORDER_BY_ID_SUCCESS,
        payload: response.data
      })
    } catch (error) {
      return dispatch({
        type: FETCH_ORDER_BY_ID_ERROR,
        error: error.message
      })
    }
  }
}

// Fetch Order Detail by ID
export const fetchApiOrderDetailByIdAction = (id) => {
  const vAPI_URL_ORDER_DETAIL = 'http://localhost:8080/api/orderDetail';

  return async (dispatch) => {
    try {
      await dispatch({
        type: FETCH_ORDER_DETAIL_BY_ID_PENDING
      })

      let response = await axios.get(vAPI_URL_ORDER_DETAIL + '/' + id);

      await dispatch(fetchApiProductByIdAction(response.data.data.product));

      return dispatch({
        type: FETCH_ORDER_DETAIL_BY_ID_SUCCESS,
        payload: response.data
      })
    } catch (error) {
      return dispatch({
        type: FETCH_ORDER_DETAIL_BY_ID_ERROR,
        error: error.message
      })
    }
  }
}

// Fetch Update Customer by ID
export const fetchApiUpdateCustomerByIdAction = (id, newInfoCustomer) => {
  const vAPI_URL_CUSTOMERS = 'http://localhost:8080/api/customer';

  return async (dispatch) => {
    try {
      await dispatch({
        type: FETCH_UPDATE_CUSTOMER_PENDING
      })

      let response = await axios.put(vAPI_URL_CUSTOMERS + '/' + id, newInfoCustomer);

      dispatch(fetchApiCustomersAction());

      return dispatch({
        type: FETCH_UPDATE_CUSTOMER_SUCCESS,
        payload: response.data
      })

    } catch (error) {
      return dispatch({
        type: FETCH_UPDATE_CUSTOMER_ERROR,
        error: error.message
      })
    }
  }
}

// Fetch Delete Customer by ID
export const fetchApiDeleteCustomerByIdAction = (id) => {
  const vAPI_URL_CUSTOMERS = 'http://localhost:8080/api/customer';

  return async (dispatch) => {
    try {
      await dispatch({
        type: FETCH_DELETE_CUSTOMER_PENDING
      })

      let response = await axios.delete(vAPI_URL_CUSTOMERS + '/' + id);

      dispatch(fetchApiCustomersAction());

      return dispatch({
        type: FETCH_DELETE_CUSTOMER_SUCCESS,
        payload: response.data
      })
    } catch (error) {
      return dispatch({
        type: FETCH_DELETE_CUSTOMER_ERROR,
        error: error.message
      })
    }
  }
}

// Create Product
export const fetchApiCreateProductAction = (newProduct) => {
  const vAPI_URL_PRODUCTS = 'http://localhost:8080/api/product';

  return async (dispatch) => {
    try {
      await dispatch({
        type: FETCH_CREATE_PRODUCT_PENDING
      })

      const headers = {
        'Content-Type': 'application/json'
      };

      let response = await axios.post(vAPI_URL_PRODUCTS, newProduct, { headers });

      return dispatch({
        type: FETCH_CREATE_PRODUCT_SUCCESS,
        payload: response.data
      })
    } catch (error) {
      return dispatch({
        type: FETCH_CREATE_PRODUCT_ERROR,
        error: error.message
      })
    }
  }
}