import {
  HANDLE_OPEN_DETAIL_MODAL,
  HANDLE_CLOSE_DETAIL_MODAL,

  HANDLE_OPEN_DELETE_MODAL,
  HANDLE_CLOSE_DELETE_MODAL,

  HANDLE_CLOSE_DETAIL_MODAL_TYPE,
  HANDLE_OPEN_DETAIL_MODAL_TYPE,

  HANDLE_CLOSE_DELETE_MODAL_TYPE,
  HANDLE_OPEN_DELETE_MODAL_TYPE,

  HANDLE_CLOSE_DETAIL_MODAL_CUSTOMER,
  HANDLE_OPEN_DETAIL_MODAL_CUSTOMER,
  HANDLE_CLOSE_CHILD_MODAL_ORDER_CUSTOMER,
  HANDLE_OPEN_CHILD_MODAL_ORDER_CUSTOMER,

  HANDLE_CLOSE_DELETE_MODAL_CUSTOMER,
  HANDLE_OPEN_DELETE_MODAL_CUSTOMER
} from '../../constants/admin/modalAdmin';

// Detail Modal Product
export const handleOpenDetailModalAction = (info) => {
  return {
    type: HANDLE_OPEN_DETAIL_MODAL,
    payload: info
  }
}
export const handleCloseDetailModalAction = () => {
  return {
    type: HANDLE_CLOSE_DETAIL_MODAL
  }
}

// Delete Modal Product
export const handleOpenDeleteModalAction = (info) => {
  return {
    type: HANDLE_OPEN_DELETE_MODAL,
    payload: info
  }
}
export const handleCloseDeleteModalAction = () => {
  return {
    type: HANDLE_CLOSE_DELETE_MODAL
  }
}

// Detail Modal Product Type
export const handleOpenDetailModalTypeAction = (info) => {
  return {
    type: HANDLE_OPEN_DETAIL_MODAL_TYPE,
    payload: info
  }
}
export const handleCloseDetailModalTypeAction = () => {
  return {
    type: HANDLE_CLOSE_DETAIL_MODAL_TYPE
  }
}

// Delete Modal Product Type
export const handleOpenDeleteModalTypeAction = (info) => {
  return {
    type: HANDLE_OPEN_DELETE_MODAL_TYPE,
    payload: info
  }
}
export const handleCloseDeleteModalTypeAction = () => {
  return {
    type: HANDLE_CLOSE_DELETE_MODAL_TYPE
  }
}

// Detail Modal Customer
export const handleOpenDetailModalCustomerAction = (info) => {
  return {
    type: HANDLE_OPEN_DETAIL_MODAL_CUSTOMER,
    payload: info
  }
}
export const handleCloseDetailModalCustomerAction = () => {
  return {
    type: HANDLE_CLOSE_DETAIL_MODAL_CUSTOMER
  }
}
// Child Modal Order (Detail Modal Customer)
export const handleOpenChildModalOrderCustomerAction = (info) => {
  return {
    type: HANDLE_OPEN_CHILD_MODAL_ORDER_CUSTOMER,
    payload: info
  }
}
export const handleCloseChildModalOrderCustomerAction = () => {
  return {
    type: HANDLE_CLOSE_CHILD_MODAL_ORDER_CUSTOMER,
  }
}

// Delete Modal Customer
export const handleOpenDeleteModalCustomerAction = (info) => {
  return {
    type: HANDLE_OPEN_DELETE_MODAL_CUSTOMER,
    payload: info
  }
}
export const handleCloseDeleteModalCustomerAction = () => {
  return {
    type: HANDLE_CLOSE_DELETE_MODAL_CUSTOMER
  }
}