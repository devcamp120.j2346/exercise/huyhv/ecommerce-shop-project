import axios from "axios";
import {
  FETCH_PRODUCT_TYPE_ERROR,
  FETCH_PRODUCT_TYPE_PENDING,
  FETCH_PRODUCT_TYPE_SUCCESS,
  FETCH_PRODUCT_TYPE_BY_ID_ERROR,
  FETCH_PRODUCT_TYPE_BY_ID_PENDING,
  FETCH_PRODUCT_TYPE_BY_ID_SUCCESS,
} from "../constants/productType.constant";
import {
  FETCH_PRODUCT_ERROR,
  FETCH_PRODUCT_PENDING,
  FETCH_PRODUCT_SUCCESS,
  FETCH_PRODUCTS_PAGINATION_ERROR,
  FETCH_PRODUCTS_PAGINATION_PENDING,
  FETCH_PRODUCTS_PAGINATION_SUCCESS,
  FETCH_PRODUCT_BY_ID_ERROR,
  FETCH_PRODUCT_BY_ID_PENDING,
  FETCH_PRODUCT_BY_ID_SUCCESS,
} from "../constants/product.constant";
import {
  HANDLE_SUBMIT_ORDER_ERROR,
  HANDLE_SUBMIT_ORDER_SUCCESS,
} from "../constants/submit.constant";
import {
  FETCH_LOGIN_ERROR,
  FETCH_LOGIN_PENDING,
  FETCH_LOGIN_SUCCESS,
  FETCH_SIGNUP_ERROR,
  FETCH_SIGNUP_PENDING,
  FETCH_SIGNUP_SUCCESS
} from '../constants/auth.constant';

// Get all List Product Type
export const fetchApiProductTypeAction = () => {
  return async (dispatch) => {
    try {
      const vAPI_URL_PRODUCT_TYPE = "http://localhost:8080/api/productType";

      const config = {
        method: "GET",
        url: vAPI_URL_PRODUCT_TYPE,
      };

      await dispatch({
        type: FETCH_PRODUCT_TYPE_PENDING,
      });

      let response = await axios.request(config);

      return dispatch({
        type: FETCH_PRODUCT_TYPE_SUCCESS,
        payload: response.data,
      });
    } catch (error) {
      return dispatch({
        type: FETCH_PRODUCT_TYPE_ERROR,
        error: error.message,
      });
    }
  };
};

// Get Product Type by ID
export const fetchApiProductTypeByIdAction = (id) => {
  return async (dispatch) => {
    try {
      const vAPI_URL_PRODUCT_TYPE = "http://localhost:8080/api/productType";

      const config = {
        method: "GET",
        url: vAPI_URL_PRODUCT_TYPE + "/" + id,
      };

      await dispatch({
        type: FETCH_PRODUCT_TYPE_BY_ID_PENDING,
      });

      let response = await axios.request(config);

      return dispatch({
        type: FETCH_PRODUCT_TYPE_BY_ID_SUCCESS,
        payload: response.data,
      });
    } catch (error) {
      return dispatch({
        type: FETCH_PRODUCT_TYPE_BY_ID_ERROR,
        error: error.message,
      });
    }
  };
};

// Get list Product by Product Type
export const fetchApiProductAction = (productTypeId, page, limit) => {
  return async (dispatch) => {
    try {
      const vAPI_URL_PRODUCT = "http://localhost:8080/api/product";

      if (productTypeId !== 0) {
        const params = new URLSearchParams({
          type: productTypeId,
        });

        const config = {
          method: "GET",
          url: vAPI_URL_PRODUCT + "?" + params.toString(),
        };

        await dispatch({
          type: FETCH_PRODUCT_PENDING,
        });

        let response = await axios.request(config);

        return dispatch({
          type: FETCH_PRODUCT_SUCCESS,
          payload: response.data,
        });
      } else {
        const params = new URLSearchParams({
          page: page,
          limit: limit,
        });

        const config = {
          method: "GET",
          url: vAPI_URL_PRODUCT + "?" + params.toString(),
        };

        await dispatch({
          type: FETCH_PRODUCT_PENDING,
        });

        let response = await axios.request(config);

        return dispatch({
          type: FETCH_PRODUCT_SUCCESS,
          payload: response.data,
        });
      }
    } catch (error) {
      return dispatch({
        type: FETCH_PRODUCT_ERROR,
        error: error.message,
      });
    }
  };
};

// Get Product By ID
export const fetchApiProductByIdAction = (id) => {
  return async (dispatch) => {
    try {
      const vAPI_URL_PRODUCT_BY_ID = "http://localhost:8080/api/product";

      let config = {
        method: "get",
        url: vAPI_URL_PRODUCT_BY_ID + "/" + id,
      };

      await dispatch({
        type: FETCH_PRODUCT_BY_ID_PENDING,
      });

      let response = await axios.request(config);

      return dispatch({
        type: FETCH_PRODUCT_BY_ID_SUCCESS,
        payload: response.data,
      });
    } catch (error) {
      return dispatch({
        type: FETCH_PRODUCT_BY_ID_ERROR,
        error: error.message,
      });
    }
  };
};

// Submit Order Customer
export const handleSubmitOrderAction = (newCustomer, note, cost, listCart) => {
  const vAPI_URL_CUSTOMER = "http://localhost:8080/api/customer/";
  const vAPI_URL_ORDER = "http://localhost:8080/api/order/";
  const vAPI_URL_ORDER_DETAIL = "http://localhost:8080/api/orderDetail/";

  return async (dispatch) => {
    try {
      const headers = {
        "Content-Type": "application/json",
      };

      // Create new Customer
      const dataCustomer = await axios.post(vAPI_URL_CUSTOMER, newCustomer, {
        headers
      });

      // Create new Order
      let newOrder = {
        note,
        cost,
        customerId: dataCustomer.data.data._id
      }
      const dataOrder = await axios.post(vAPI_URL_ORDER, newOrder, { headers })

      // Push Order Detail List
      listCart.map(async (item, index) => {
        let orderDetail = {
          product: item._id,
          quantity: item.quantity,
          orderId: dataOrder.data.data._id
        }

        await axios.post(vAPI_URL_ORDER_DETAIL, orderDetail, { headers });
      })

      return dispatch({
        type: HANDLE_SUBMIT_ORDER_SUCCESS,
        dataCustomer,
        dataOrder
      })

    } catch (error) {
      return dispatch({
        type: HANDLE_SUBMIT_ORDER_ERROR,
        error: error.message,
      });
    }
  };
};

// Log In
export const fetchApiLoginAction = (user, navigate) => {
  const vAPI_URL_LOGIN = 'http://localhost:8080/api/auth/login';

  return async (dispatch) => {

    try {
      const headers = {
        'Content-Type': 'application/json'
      };

      await dispatch({
        type: FETCH_LOGIN_PENDING
      })

      let response = await axios.post(vAPI_URL_LOGIN, user, { headers });
      navigate('/');

      return dispatch({
        type: FETCH_LOGIN_SUCCESS,
        payload: response.data
      })

    } catch (error) {
      return dispatch({
        type: FETCH_LOGIN_ERROR,
        error: error.message
      })
    }
  }
}

// Sign Up
export const fetchApiSignUpAction = (user, navigate) => {
  const vAPI_URL_SIGN_UP = 'http://localhost:8080/api/auth/signup';

  return async (dispatch) => {
    try {
      const headers = {
        'Content-Type': 'application/json'
      };

      await dispatch({
        type: FETCH_SIGNUP_PENDING
      })

      let response = await axios.post(vAPI_URL_SIGN_UP, user, { headers });
      navigate('/login');

      return dispatch({
        type: FETCH_SIGNUP_SUCCESS,
        payload: response.data
      })

    } catch (error) {
      return dispatch({
        type: FETCH_SIGNUP_ERROR,
        error: error.message
      })
    }
  }
}


// Get all Product (Pagination)
// export const getAllProductsAction = (page, limit) => {

//   return async(dispatch) => {
//     const vAPI_URL_PRODUCT = 'http://localhost:8080/api/product';

//     try {

//     } catch (error) {
//       return dispatch({
//         type: FETCH_PRODUCTS_PAGINATION_ERROR,
//         error: error.message
//       })
//     }

//   }
// }
