import { HANDLE_REMOVE_ITEM_CART } from '../constants/product.constant';

export const handleRemoveItemCartAction = (index) => {
  return {
    type: HANDLE_REMOVE_ITEM_CART,
    payload: index
  }
}