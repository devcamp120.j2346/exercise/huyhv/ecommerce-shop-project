import { HANDLE_ADD_CART } from '../constants/addCart.constant';

export const handleAddCartAction = (item) => {
  return {
    type: HANDLE_ADD_CART,
    payload: item
  }
}