import { HANDLE_CHANGE_PAGINATION } from '../constants/pagination.constant';

export const handleChangePaginationAction = (pageValue) => {
  return {
    type: HANDLE_CHANGE_PAGINATION,
    payload: pageValue
  }
}