import {
  HANDLE_CHANGE_ADDRESS,
  HANDLE_CHANGE_CITY,
  HANDLE_CHANGE_COMPANY_NAME,
  HANDLE_CHANGE_COUNTRY,
  HANDLE_CHANGE_EMAIL,
  HANDLE_CHANGE_FIRST_NAME,
  HANDLE_CHANGE_LAST_NAME,
  HANDLE_CHANGE_NOTE,
  HANDLE_CHANGE_PHONE
} from '../constants/changeInformation.constant';

export const handleChangeFirstNameAction = (inputFirstname) => {
  return {
    type: HANDLE_CHANGE_FIRST_NAME,
    payload: inputFirstname
  }
}
export const handleChangeLastNameAction = (inputLastname) => {
  return {
    type: HANDLE_CHANGE_LAST_NAME,
    payload: inputLastname
  }
}
export const handleChangeCompanyNameAction = (inputCompanyname) => {
  return {
    type: HANDLE_CHANGE_COMPANY_NAME,
    payload: inputCompanyname
  }
}
export const handleChangeAddressAction = (inputAddress) => {
  return {
    type: HANDLE_CHANGE_ADDRESS,
    payload: inputAddress
  }
}
export const handleChangeCountryAction = (inputCountry) => {
  return {
    type: HANDLE_CHANGE_COUNTRY,
    payload: inputCountry
  }
}
export const handleChangeCityAction = (inputCity) => {
  return {
    type: HANDLE_CHANGE_CITY,
    payload: inputCity
  }
}
export const handleChangeEmailAction = (inputEmail) => {
  return {
    type: HANDLE_CHANGE_EMAIL,
    payload: inputEmail
  }
}
export const handleChangePhoneAction = (inputPhone) => {
  return {
    type: HANDLE_CHANGE_PHONE,
    payload: inputPhone
  }
}
export const handleChangeNoteAction = (inputNote) => {
  return {
    type: HANDLE_CHANGE_NOTE,
    payload: inputNote
  }
}