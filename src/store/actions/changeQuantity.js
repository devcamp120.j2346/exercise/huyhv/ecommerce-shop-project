import {
  HANDLE_INCREASE_QUANTITY,
  HANDLE_DESCREASE_QUANTITY,
  HANDLE_CHANGE_QUANTITY,
  HANDLE_DESC_QUANTITY_CART,
  HANDLE_INC_QUANTITY_CART
} from '../constants/product.constant'

export const handleIncreaseQuantityAction = () => {
  return {
    type: HANDLE_INCREASE_QUANTITY
  }
}
export const handleDescreaseQuantityAction = () => {
  return {
    type: HANDLE_DESCREASE_QUANTITY
  }
}
export const handleChangeQuantityAction = (inputValue) => {
  return {
    type: HANDLE_CHANGE_QUANTITY,
    payload: inputValue
  }
}
export const handleIncQuantityCartAction = (index) => {
  return {
    type: HANDLE_INC_QUANTITY_CART,
    payload: index
  }
}
export const handleDescQuantityCartAction = (index) => {
  return {
    type: HANDLE_DESC_QUANTITY_CART,
    payload: index
  }
}