import userComment1 from '../assets/images/customer/user1.png';
import userComment2 from '../assets/images/customer/user2.png';
import userComment3 from '../assets/images/customer/user3.png';

export const customerFeedback = [
  {
    id: 1,
    avatar: userComment1,
    name: 'Kristin Watson',
    time: '2 min ago',
    comment: 'Duis at ullamcorper nulla, eu dictum eros.',
  },
  {
    id: 2,
    avatar: userComment2,
    name: 'Jacob Jones',
    time: '2 min ago',
    comment: ' Vivamus eget euismod magna. Nam sed lacinia nibh, et lacinia lacus.',
  },
  {
    id: 3,
    avatar: userComment3,
    name: 'Ralph Edwards',
    time: '2 min ago',
    comment: '200+ Canton Pak Choi Bok Choy Chinese Cabbage Seeds Heirloom Non-GMO Productive Brassica rapa VAR. chinensis, a.k.a. Cantons Choice, Bok Choi, from USA',
  },
]