// Get all Product
export const FETCH_PRODUCT_PENDING = 'FETCH_PRODUCT_PENDING';
export const FETCH_PRODUCT_SUCCESS = 'FETCH_PRODUCT_SUCCESS';
export const FETCH_PRODUCT_ERROR = 'FETCH_PRODUCT_ERROR';

// Get Product by ID
export const FETCH_PRODUCT_BY_ID_PENDING = 'FETCH_PRODUCT_BY_ID_PENDING';
export const FETCH_PRODUCT_BY_ID_SUCCESS = 'FETCH_PRODUCT_BY_ID_SUCCESS';
export const FETCH_PRODUCT_BY_ID_ERROR = 'FETCH_PRODUCT_BY_ID_ERROR';

// Get all Product (Pagination)
export const FETCH_PRODUCTS_PAGINATION_PENDING = 'FETCH_PRODUCTS_PAGINATION_PENDING';
export const FETCH_PRODUCTS_PAGINATION_SUCCESS = 'FETCH_PRODUCTS_PAGINATION_SUCCESS';
export const FETCH_PRODUCTS_PAGINATION_ERROR = 'FETCH_PRODUCTS_PAGINATION_ERROR';

// Increase & Descrease Quantity
export const HANDLE_DESCREASE_QUANTITY = 'HANDLE_DESCREASE_QUANTITY';
export const HANDLE_INCREASE_QUANTITY = 'HANDLE_INCREASE_QUANTITY';
export const HANDLE_CHANGE_QUANTITY = 'HANDLE_CHANGE_QUANTITY';

// Increase & Descrease Quantity in Cart
export const HANDLE_INC_QUANTITY_CART = 'HANDLE_INC_QUANTITY_CART';
export const HANDLE_DESC_QUANTITY_CART = 'HANDLE_DESC_QUANTITY_CART';

// Remove Item in Cart
export const HANDLE_REMOVE_ITEM_CART = 'HANDLE_REMOVE_ITEM_CART';