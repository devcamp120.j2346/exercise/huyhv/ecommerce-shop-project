import {
  FETCH_LOGIN_ERROR,
  FETCH_LOGIN_PENDING,
  FETCH_LOGIN_SUCCESS,
  FETCH_SIGNUP_ERROR,
  FETCH_SIGNUP_PENDING,
  FETCH_SIGNUP_SUCCESS
} from '../constants/auth.constant';

const initState = {

  // Login
  user: {
    pending: false,
    userInfo: null,
    isError: false,
    accessToken: ''
  },

  // Sign up
  signUp: {
    pending: false,
    isError: false,
    message: ''
  },
}

const authReducer = (state = initState, action) => {

  switch (action.type) {

    // Login
    case FETCH_LOGIN_PENDING:
      state.user.pending = true;
      break;
    case FETCH_LOGIN_ERROR:
      state.user.pending = false;
      state.user.isError = true;
      break;
    case FETCH_LOGIN_SUCCESS:
      state.user.pending = false;
      state.user.isError = false;
      state.user.userInfo = action.payload;
      state.user.accessToken = action.payload.accessToken;
      break

    // Sign up
    case FETCH_SIGNUP_PENDING:
      state.signUp.pending = true;
      break;
    case FETCH_SIGNUP_ERROR:
      state.signUp.pending = false;
      state.signUp.isError = true;
      break;
    case FETCH_SIGNUP_SUCCESS:
      state.signUp.pending = false;
      state.signUp.isError = false;
      state.signUp.message = action.payload;
      break;

    default:
      break;
  }


  return { ...state }
}

export default authReducer