import { HANDLE_CHANGE_PAGINATION } from '../constants/pagination.constant';

const initState = {
  // Pagination
  currentPage: 1,
  limit: 9
}

const paginationReducer = (state = initState, action) => {
  switch (action.type) {
    case HANDLE_CHANGE_PAGINATION:
      state.currentPage = action.payload;
      break;

    default:
      break;
  }

  return { ...state };
}

export default paginationReducer