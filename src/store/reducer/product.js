import {
  FETCH_PRODUCT_TYPE_ERROR,
  FETCH_PRODUCT_TYPE_PENDING,
  FETCH_PRODUCT_TYPE_SUCCESS,
  FETCH_PRODUCT_TYPE_BY_ID_ERROR,
  FETCH_PRODUCT_TYPE_BY_ID_PENDING,
  FETCH_PRODUCT_TYPE_BY_ID_SUCCESS
} from '../constants/productType.constant';
import {
  FETCH_PRODUCT_ERROR,
  FETCH_PRODUCT_PENDING,
  FETCH_PRODUCT_SUCCESS,
  FETCH_PRODUCT_BY_ID_ERROR,
  FETCH_PRODUCT_BY_ID_PENDING,
  FETCH_PRODUCT_BY_ID_SUCCESS,
  HANDLE_DESCREASE_QUANTITY,
  HANDLE_INCREASE_QUANTITY,
  HANDLE_CHANGE_QUANTITY,
  HANDLE_DESC_QUANTITY_CART,
  HANDLE_INC_QUANTITY_CART,
  HANDLE_REMOVE_ITEM_CART
} from '../constants/product.constant';
import { HANDLE_ADD_CART } from '../constants/addCart.constant';
import {
  HANDLE_CHANGE_ADDRESS,
  HANDLE_CHANGE_CITY,
  HANDLE_CHANGE_COMPANY_NAME,
  HANDLE_CHANGE_COUNTRY,
  HANDLE_CHANGE_EMAIL,
  HANDLE_CHANGE_FIRST_NAME,
  HANDLE_CHANGE_LAST_NAME,
  HANDLE_CHANGE_NOTE,
  HANDLE_CHANGE_PHONE
} from '../constants/changeInformation.constant';
import { HANDLE_SUBMIT_ORDER_ERROR, HANDLE_SUBMIT_ORDER_SUCCESS } from '../constants/submit.constant';

const storageListCart = JSON.parse(localStorage.getItem('listCart'));

const initState = {

  // List Product Type
  productType: {
    pending: false,
    listProductType: [],
    isError: false,
  },

  // Product Type by ID
  productTypeByID: {
    pending: false,
    productTypeByID: {},
    isError: false
  },

  // List Product
  product: {
    pending: false,
    totalProduct: 0,
    listProduct: [],
    isError: false
  },

  // Product by ID
  productById: {
    pending: false,
    product: {},
    isError: false
  },

  // List Cart
  listCart: storageListCart ? storageListCart : [],
  subTotal: storageListCart ? storageListCart.reduce((sum, item) => {
    return sum + (item.promotionPrice * item.quantity)
  }, 0) : 0,

  // User Information
  customer: {
    firstName: '',
    lastName: '',
    company: '',
    address: '',
    country: '',
    city: '',
    email: '',
    phone: '',
    note: ''
  }
}

const productReducer = (state = initState, action) => {
  switch (action.type) {

    // List Product Type
    case FETCH_PRODUCT_TYPE_PENDING:
      state.productType.pending = true;
      break;
    case FETCH_PRODUCT_TYPE_ERROR:
      state.productType.pending = false;
      state.productType.isError = true;
      break;
    case FETCH_PRODUCT_TYPE_SUCCESS:
      state.productType.pending = false;
      state.productType.isError = false;
      state.productType.listProductType = action.payload;
      break;

    // Get Product Type by ID
    case FETCH_PRODUCT_TYPE_BY_ID_PENDING:
      state.productTypeByID.pending = true;
      break;
    case FETCH_PRODUCT_TYPE_BY_ID_ERROR:
      state.productTypeByID.pending = false;
      state.productTypeByID.isError = true;
      break;
    case FETCH_PRODUCT_TYPE_BY_ID_SUCCESS:
      state.productTypeByID.pending = false;
      state.productTypeByID.isError = false;
      state.productTypeByID.productTypeByID = action.payload;
      break;

    // List Product
    case FETCH_PRODUCT_PENDING:
      state.product.pending = true
      break;
    case FETCH_PRODUCT_ERROR:
      state.product.pending = false;
      state.product.isError = true;
      break;
    case FETCH_PRODUCT_SUCCESS:
      state.product.pending = false;
      state.product.isError = false;
      state.product.totalProduct = action.payload.total;
      state.product.listProduct = action.payload.data.map(item => {
        return { ...item, quantity: 1 };
      });
      break;

    // Product By ID
    case FETCH_PRODUCT_BY_ID_PENDING:
      state.productById.pending = true;
      break;
    case FETCH_PRODUCT_BY_ID_ERROR:
      state.productById.pending = false;
      state.productById.isError = true;
      break;

    case FETCH_PRODUCT_BY_ID_SUCCESS:
      state.productById.pending = false;
      state.productById.isError = false;
      state.productById.product = action.payload;
      state.productById.product.data.quantity = 1;
      break;

    // Handle Change Quantity
    case HANDLE_INCREASE_QUANTITY:
      state.productById.product.data.quantity = state.productById.product.data.quantity + 1
      break;
    case HANDLE_DESCREASE_QUANTITY:
      state.productById.product.data.quantity = state.productById.product.data.quantity - 1;
      if (state.productById.product.data.quantity < 1) {
        state.productById.product.data.quantity = 1;
      }
      break;
    case HANDLE_CHANGE_QUANTITY:
      state.productById.product.data.quantity = action.payload;
      break;

    // Handle Change Quantity in Cart
    case HANDLE_DESC_QUANTITY_CART:
      if (state.listCart[action.payload].quantity > 1) {
        state.listCart[action.payload].quantity = state.listCart[action.payload].quantity - 1;
      }

      // Calculate subTotal
      state.subTotal = state.listCart.reduce((sum, item) => {
        return sum + (item.promotionPrice * item.quantity)
      }, 0)

      // Save localStorage
      localStorage.setItem('listCart', JSON.stringify(state.listCart));
      break;
    case HANDLE_INC_QUANTITY_CART:
      state.listCart[action.payload].quantity = state.listCart[action.payload].quantity + 1;

      // Calculate subTotal
      state.subTotal = state.listCart.reduce((sum, item) => {
        return sum + (item.promotionPrice * item.quantity)
      }, 0)

      // Save localStorage
      localStorage.setItem('listCart', JSON.stringify(state.listCart));
      break;

    // Handle Add Cart
    case HANDLE_ADD_CART:
      state.listCart.push(action.payload);
      localStorage.setItem('listCart', JSON.stringify(state.listCart));
      state.subTotal = state.listCart.reduce((sum, item) => {
        return sum + (item.promotionPrice * item.quantity)
      }, 0)
      break;

    // Handle Remove Item in Cart
    case HANDLE_REMOVE_ITEM_CART:
      // console.log('remove', action.payload)
      state.listCart.splice(action.payload, 1);

      // Save localStorage
      localStorage.setItem('listCart', JSON.stringify(state.listCart));

      // Calculate subTotal
      state.subTotal = state.listCart.reduce((sum, item) => {
        return sum + (item.promotionPrice * item.quantity)
      }, 0)
      break;

    // Handle Change Customer Information
    case HANDLE_CHANGE_FIRST_NAME:
      state.customer.firstName = action.payload;
      break;
    case HANDLE_CHANGE_LAST_NAME:
      state.customer.lastName = action.payload;
      break;
    case HANDLE_CHANGE_COMPANY_NAME:
      state.customer.company = action.payload;
      break;
    case HANDLE_CHANGE_ADDRESS:
      state.customer.address = action.payload;
      break;
    case HANDLE_CHANGE_COUNTRY:
      state.customer.country = action.payload;
      break;
    case HANDLE_CHANGE_CITY:
      state.customer.city = action.payload;
      break;
    case HANDLE_CHANGE_EMAIL:
      state.customer.email = action.payload;
      break;
    case HANDLE_CHANGE_PHONE:
      state.customer.phone = action.payload;
      break;
    case HANDLE_CHANGE_NOTE:
      state.customer.note = action.payload;
      break;

    // Handle Submit Order
    case HANDLE_SUBMIT_ORDER_SUCCESS:
      console.log(action.dataCustomer);
      // console.log(action.dataOrder);
      console.log(action.newOrder);
      break;

    default:
      break;
  }

  return { ...state }
}

export default productReducer;