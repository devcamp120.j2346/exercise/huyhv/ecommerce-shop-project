import { combineReducers } from "redux";
import productReducer from './product';
import paginationReducer from "./pagination";
import authReducer from "./auth";
import adminReducer from './admin';

const rootReducer = combineReducers({
  productReducer,
  paginationReducer,
  authReducer,
  adminReducer
})

export default rootReducer;