import {
  // Fetch List Product
  FETCH_PRODUCTS_ERROR,
  FETCH_PRODUCTS_PENDING,
  FETCH_PRODUCTS_SUCCESS,

  // Fetch Product by ID
  FETCH_PRODUCT_BY_ID_ERROR,
  FETCH_PRODUCT_BY_ID_PENDING,
  FETCH_PRODUCT_BY_ID_SUCCESS,

  // Fetch List Product Type
  FETCH_PRODUCT_TYPE_ERROR,
  FETCH_PRODUCT_TYPE_PENDING,
  FETCH_PRODUCT_TYPE_SUCCESS,

  // Fetch Delete Product by ID
  FETCH_DELETE_PRODUCT_BY_ID_ERROR,
  FETCH_DELETE_PRODUCT_BY_ID_PENDING,
  FETCH_DELETE_PRODUCT_BY_ID_SUCCESS,

  // Fetch Update Product by ID
  FETCH_UPDATE_PRODUCT_BY_ID_ERROR,
  FETCH_UPDATE_PRODUCT_BY_ID_PENDING,
  FETCH_UPDATE_PRODUCT_BY_ID_SUCCESS,

  // Fetch Update Type by ID
  FETCH_UPDATE_TYPE_BY_ID_ERROR,
  FETCH_UPDATE_TYPE_BY_ID_PENDING,
  FETCH_UPDATE_TYPE_BY_ID_SUCCESS,

  // Fetch Delete Type by ID
  FETCH_DELETE_TYPE_BY_ID_ERROR,
  FETCH_DELETE_TYPE_BY_ID_PENDING,
  FETCH_DELETE_TYPE_BY_ID_SUCCESS,

  // Change Info Product
  HANDLE_CHANGE_AMOUNT_PRODUCT,
  HANDLE_CHANGE_BUY_PRICE_PRODUCT,
  HANDLE_CHANGE_DESC_PRODUCT,
  HANDLE_CHANGE_IMAGE_URL_PRODUCT,
  HANDLE_CHANGE_NAME_PRODUCT,
  HANDLE_CHANGE_PROMOTION_PRICE_PRODUCT,
  HANDLE_CHANGE_TYPE_PRODUCT,

  // Change Info Product Type
  HANDLE_CHANGE_DESC_TYPE,
  HANDLE_CHANGE_NAME_TYPE,

  // Fetch Order by ID
  FETCH_ORDER_BY_ID_ERROR,
  FETCH_ORDER_BY_ID_PENDING,
  FETCH_ORDER_BY_ID_SUCCESS,

  // Fetch Order Detail by ID
  FETCH_ORDER_DETAIL_BY_ID_ERROR,
  FETCH_ORDER_DETAIL_BY_ID_PENDING,
  FETCH_ORDER_DETAIL_BY_ID_SUCCESS,

  // Create Product
  FETCH_CREATE_PRODUCT_ERROR,
  FETCH_CREATE_PRODUCT_PENDING,
  FETCH_CREATE_PRODUCT_SUCCESS
} from '../constants/admin/productAdmin';
import {
  HANDLE_OPEN_DETAIL_MODAL,
  HANDLE_CLOSE_DETAIL_MODAL,

  HANDLE_OPEN_DELETE_MODAL,
  HANDLE_CLOSE_DELETE_MODAL,

  HANDLE_CLOSE_DETAIL_MODAL_TYPE,
  HANDLE_OPEN_DETAIL_MODAL_TYPE,

  HANDLE_CLOSE_DELETE_MODAL_TYPE,
  HANDLE_OPEN_DELETE_MODAL_TYPE,

  HANDLE_CLOSE_DETAIL_MODAL_CUSTOMER,
  HANDLE_OPEN_DETAIL_MODAL_CUSTOMER,
  HANDLE_CLOSE_CHILD_MODAL_ORDER_CUSTOMER,
  HANDLE_OPEN_CHILD_MODAL_ORDER_CUSTOMER,

  HANDLE_CLOSE_DELETE_MODAL_CUSTOMER,
  HANDLE_OPEN_DELETE_MODAL_CUSTOMER
} from '../constants/admin/modalAdmin';
import {
  FETCH_CUSTOMERS_ERROR,
  FETCH_CUSTOMERS_PENDING,
  FETCH_CUSTOMERS_SUCCESS,

  // Change info Customer
  HANDLE_CHANGE_ADDRESS_CUSTOMER,
  HANDLE_CHANGE_CITY_CUSTOMER,
  HANDLE_CHANGE_COUNTRY_CUSTOMER,
  HANDLE_CHANGE_EMAIL_CUSTOMER,
  HANDLE_CHANGE_FULLNAME_CUSTOMER,
  HANDLE_CHANGE_PHONE_CUSTOMER,

  // Fetch Update Customer by ID
  FETCH_UPDATE_CUSTOMER_ERROR,
  FETCH_UPDATE_CUSTOMER_PENDING,
  FETCH_UPDATE_CUSTOMER_SUCCESS,

  // Fetch Delete Customer by ID
  FETCH_DELETE_CUSTOMER_ERROR,
  FETCH_DELETE_CUSTOMER_PENDING,
  FETCH_DELETE_CUSTOMER_SUCCESS
} from '../constants/admin/customerAdmin';

const initState = {

  // List Product
  product: {
    pending: false,
    listProduct: null,
    isError: false
  },

  // List Product Type
  productType: {
    pending: false,
    listProductType: null,
    isError: false
  },

  // Update Product
  updateProduct: {
    pending: false,
    isError: false,
    message: ''
  },

  // Delete Product
  deleteProduct: {
    pending: false,
    isError: false,
    message: ''
  },

  // Detail Modal Product
  detailModal: {
    open: false,
    id: '',
    name: '',
    description: '',
    type: '',
    imageUrl: '',
    buyPrice: 0,
    promotionPrice: 0,
    amount: 0,
  },

  // Delete Modal Product
  deleteModal: {
    open: false,
    id: ''
  },

  // Detail Modal Type
  detailModalType: {
    open: false,
    id: '',
    name: '',
    description: ''
  },

  // Delete Modal Type
  deleteModalType: {
    open: false,
    id: ''
  },

  // Update Type
  updateType: {
    pending: false,
    isError: false,
    message: ''
  },

  // Delete Type
  deleteType: {
    pending: false,
    isError: false,
    message: ''
  },

  // Fetch List Customer
  customer: {
    pending: false,
    isError: false,
    listCustomers: null
  },

  // Detail Modal Customer
  detailModalCustomer: {
    open: false,
    id: '',
    fullName: '',
    phone: '',
    email: '',
    address: '',
    city: '',
    country: '',
    orders: [],

    childModalOrder: {
      open: false,
      pending: false,
      isError: false,
      listOrderCustomer: [],
      listOrderDetail: [],
      listOrderProduct: []
    }
  },

  // Update Customer
  updateCustomer: {
    pending: false,
    isError: false,
    message: ''
  },

  // Delete Modal Customer
  deleteModalCustomer: {
    open: false,
    id: ''
  },

  // Delete Customer
  deleteCustomer: {
    pending: false,
    isError: false,
    message: ''
  },

  // Create Product
  createProduct: {
    pending: false,
    isError: false,
    message: ''
  }
}

const adminReducer = (state = initState, action) => {
  switch (action.type) {

    // List Products
    case FETCH_PRODUCTS_PENDING:
      state.product.pending = true;
      break;

    case FETCH_PRODUCTS_ERROR:
      state.product.pending = false;
      state.product.isError = true;
      break;

    case FETCH_PRODUCTS_SUCCESS:
      state.product.pending = false;
      state.product.isError = false;
      state.product.listProduct = action.payload;
      break;

    // List Product Type
    case FETCH_PRODUCT_TYPE_PENDING:
      state.productType.pending = true;
      break;
    case FETCH_PRODUCT_TYPE_ERROR:
      state.productType.pending = false;
      state.productType.isError = true;
      break;
    case FETCH_PRODUCT_TYPE_SUCCESS:
      state.productType.pending = false;
      state.productType.isError = false;
      state.productType.listProductType = action.payload;
      break;

    // Delete Product
    case FETCH_DELETE_PRODUCT_BY_ID_PENDING:
      state.deleteProduct.pending = true;
      break;
    case FETCH_DELETE_PRODUCT_BY_ID_ERROR:
      state.deleteProduct.pending = false;
      state.deleteProduct.isError = true;
      break;
    case FETCH_DELETE_PRODUCT_BY_ID_SUCCESS:
      state.deleteProduct.pending = false;
      state.deleteProduct.isError = false;
      state.deleteProduct.message = action.payload;

      // Hide Delete Modal
      state.deleteModal.open = false;
      break;

    // Update Product
    case FETCH_UPDATE_PRODUCT_BY_ID_PENDING:
      state.updateProduct.pending = true;
      break;
    case FETCH_UPDATE_PRODUCT_BY_ID_ERROR:
      state.updateProduct.pending = false;
      state.updateProduct.isError = true;
      break;
    case FETCH_UPDATE_PRODUCT_BY_ID_SUCCESS:
      state.updateProduct.pending = false;
      state.updateProduct.isError = false;
      state.updateProduct.message = action.payload;

      // Hide Detail Modal
      state.detailModal.open = false;
      break;

    // Detail Modal
    case HANDLE_OPEN_DETAIL_MODAL:
      state.detailModal.open = true;

      // Show info
      state.detailModal.id = action.payload._id;
      state.detailModal.name = action.payload.name;
      state.detailModal.description = action.payload.description;
      state.detailModal.type = action.payload.type;
      state.detailModal.imageUrl = action.payload.imageUrl;
      state.detailModal.buyPrice = action.payload.buyPrice;
      state.detailModal.promotionPrice = action.payload.promotionPrice;
      state.detailModal.amount = action.payload.amount;
      break;
    case HANDLE_CLOSE_DETAIL_MODAL:
      state.detailModal.open = false;

      // Hide info
      state.detailModal.id = '';
      state.detailModal.name = '';
      state.detailModal.description = '';
      state.detailModal.type = '';
      state.detailModal.imageUrl = '';
      state.detailModal.buyPrice = '';
      state.detailModal.promotionPrice = '';
      state.detailModal.amount = '';
      break

    // Change Info in Detail Modal Product
    case HANDLE_CHANGE_NAME_PRODUCT:
      state.detailModal.name = action.payload;
      break;
    case HANDLE_CHANGE_DESC_PRODUCT:
      state.detailModal.description = action.payload;
      break;
    case HANDLE_CHANGE_TYPE_PRODUCT:
      state.detailModal.type = action.payload;
      break;
    case HANDLE_CHANGE_IMAGE_URL_PRODUCT:
      state.detailModal.imageUrl = action.payload;
      break;
    case HANDLE_CHANGE_BUY_PRICE_PRODUCT:
      state.detailModal.buyPrice = action.payload;
      break;
    case HANDLE_CHANGE_PROMOTION_PRICE_PRODUCT:
      state.detailModal.promotionPrice = action.payload;
      break;
    case HANDLE_CHANGE_AMOUNT_PRODUCT:
      state.detailModal.amount = action.payload;
      break;

    // Delete Modal
    case HANDLE_OPEN_DELETE_MODAL:
      state.deleteModal.open = true;
      state.deleteModal.id = action.payload._id;
      break;
    case HANDLE_CLOSE_DELETE_MODAL:
      state.deleteModal.open = false;
      break;

    // Detail Modal Product Type
    case HANDLE_OPEN_DETAIL_MODAL_TYPE:
      state.detailModalType.open = true;
      state.detailModalType.id = action.payload._id;
      state.detailModalType.name = action.payload.name;
      state.detailModalType.description = action.payload.description;
      break;
    case HANDLE_CLOSE_DETAIL_MODAL_TYPE:
      state.detailModalType.open = false;
      state.detailModalType.id = '';
      state.detailModalType.name = '';
      state.detailModalType.description = '';
      break;

    // Change Info in Detail Modal Product Type
    case HANDLE_CHANGE_NAME_TYPE:
      state.detailModalType.name = action.payload;
      break;
    case HANDLE_CHANGE_DESC_TYPE:
      state.detailModalType.description = action.payload;
      break;

    // Delete Modal Product Type
    case HANDLE_OPEN_DELETE_MODAL_TYPE:
      state.deleteModalType.open = true;
      state.deleteModalType.id = action.payload._id;
      break;
    case HANDLE_CLOSE_DELETE_MODAL_TYPE:
      state.deleteModalType.open = false;
      break;

    // Update Type by ID
    case FETCH_UPDATE_TYPE_BY_ID_PENDING:
      state.updateType.pending = true;
      break;
    case FETCH_UPDATE_TYPE_BY_ID_ERROR:
      state.updateType.pending = false;
      state.updateType.isError = true;
      break;
    case FETCH_UPDATE_TYPE_BY_ID_SUCCESS:
      state.updateType.pending = false;
      state.updateType.isError = false;
      state.updateType.message = action.payload;
      state.detailModalType.open = false;
      break;

    // Delete Type by ID
    case FETCH_DELETE_TYPE_BY_ID_PENDING:
      state.deleteType.pending = true;
      break;
    case FETCH_DELETE_TYPE_BY_ID_ERROR:
      state.deleteType.pending = false;
      state.deleteType.isError = true;
      break;
    case FETCH_DELETE_TYPE_BY_ID_SUCCESS:
      state.deleteType.pending = false;
      state.deleteType.isError = false;
      state.deleteType.message = action.payload;
      state.deleteModalType.open = false;
      break;

    // Fetch List Customers
    case FETCH_CUSTOMERS_PENDING:
      state.customer.pending = true;
      break;
    case FETCH_CUSTOMERS_ERROR:
      state.customer.pending = false;
      state.customer.isError = true;
      break;
    case FETCH_CUSTOMERS_SUCCESS:
      state.customer.pending = false;
      state.customer.isError = false;
      state.customer.listCustomers = action.payload;
      break;

    // Detail Modal Customer
    case HANDLE_OPEN_DETAIL_MODAL_CUSTOMER:
      state.detailModalCustomer.open = true;
      state.detailModalCustomer.id = action.payload._id;
      state.detailModalCustomer.fullName = action.payload.fullName;
      state.detailModalCustomer.phone = action.payload.phone;
      state.detailModalCustomer.email = action.payload.email;
      state.detailModalCustomer.address = action.payload.address;
      state.detailModalCustomer.city = action.payload.city;
      state.detailModalCustomer.country = action.payload.country;
      state.detailModalCustomer.orders = action.payload.orders;
      break;
    case HANDLE_CLOSE_DETAIL_MODAL_CUSTOMER:
      state.detailModalCustomer.open = false;
      break;

    // Child Modal Order (Detail Modal Customer)
    case HANDLE_OPEN_CHILD_MODAL_ORDER_CUSTOMER:
      state.detailModalCustomer.childModalOrder.open = true;
      break;
    case HANDLE_CLOSE_CHILD_MODAL_ORDER_CUSTOMER:
      state.detailModalCustomer.childModalOrder.open = false;
      state.detailModalCustomer.childModalOrder.listOrderCustomer = [];
      state.detailModalCustomer.childModalOrder.listOrderDetail = [];
      state.detailModalCustomer.childModalOrder.listOrderProduct = [];
      break;

    // Fetch Order by ID
    case FETCH_ORDER_BY_ID_PENDING:
      state.detailModalCustomer.childModalOrder.pending = true;
      break;
    case FETCH_ORDER_BY_ID_ERROR:
      state.detailModalCustomer.childModalOrder.pending = false;
      state.detailModalCustomer.childModalOrder.isError = true;
      break;
    case FETCH_ORDER_BY_ID_SUCCESS:
      state.detailModalCustomer.childModalOrder.pending = false;
      state.detailModalCustomer.childModalOrder.isError = false;
      state.detailModalCustomer.childModalOrder.listOrderCustomer = [...state.detailModalCustomer.childModalOrder.listOrderCustomer, action.payload];
      break;

    // Fetch Order Detail by ID
    case FETCH_ORDER_DETAIL_BY_ID_PENDING:
      break;
    case FETCH_ORDER_DETAIL_BY_ID_ERROR:
      break;
    case FETCH_ORDER_DETAIL_BY_ID_SUCCESS:
      state.detailModalCustomer.childModalOrder.listOrderDetail = [...state.detailModalCustomer.childModalOrder.listOrderDetail, action.payload];
      break;

    // Fetch Product by ID
    case FETCH_PRODUCT_BY_ID_PENDING:
      break;
    case FETCH_PRODUCT_BY_ID_ERROR:
      break;
    case FETCH_PRODUCT_BY_ID_SUCCESS:
      state.detailModalCustomer.childModalOrder.listOrderProduct = [...state.detailModalCustomer.childModalOrder.listOrderProduct, action.payload];
      break;

    // Change info Customer
    case HANDLE_CHANGE_FULLNAME_CUSTOMER:
      state.detailModalCustomer.fullName = action.payload;
      break;
    case HANDLE_CHANGE_PHONE_CUSTOMER:
      state.detailModalCustomer.phone = action.payload;
      break;
    case HANDLE_CHANGE_EMAIL_CUSTOMER:
      state.detailModalCustomer.email = action.payload;
      break;
    case HANDLE_CHANGE_ADDRESS_CUSTOMER:
      state.detailModalCustomer.address = action.payload;
      break;
    case HANDLE_CHANGE_CITY_CUSTOMER:
      state.detailModalCustomer.city = action.payload;
      break;
    case HANDLE_CHANGE_COUNTRY_CUSTOMER:
      state.detailModalCustomer.country = action.payload;
      break;

    // Fetch Update Customer by ID
    case FETCH_UPDATE_CUSTOMER_PENDING:
      state.updateCustomer.pending = true;
      break;
    case FETCH_UPDATE_CUSTOMER_ERROR:
      state.updateCustomer.pending = false;
      state.updateCustomer.isError = true;
      break;
    case FETCH_UPDATE_CUSTOMER_SUCCESS:
      state.updateCustomer.pending = false;
      state.updateCustomer.isError = false;
      state.updateCustomer.message = action.payload;
      state.detailModalCustomer.open = false;
      break;

    // Delete Modal Customer
    case HANDLE_OPEN_DELETE_MODAL_CUSTOMER:
      state.deleteModalCustomer.open = true;
      state.deleteModalCustomer.id = action.payload._id;
      break;
    case HANDLE_CLOSE_DELETE_MODAL_CUSTOMER:
      state.deleteModalCustomer.open = false;
      break;

    // Fetch Delete Customer by ID
    case FETCH_DELETE_CUSTOMER_PENDING:
      state.deleteCustomer.pending = true;
      break;
    case FETCH_DELETE_CUSTOMER_ERROR:
      state.deleteCustomer.pending = false;
      state.deleteCustomer.isError = true;
      break;
    case FETCH_DELETE_CUSTOMER_SUCCESS:
      state.deleteCustomer.pending = false;
      state.deleteCustomer.isError = false;
      state.deleteCustomer.message = action.payload;
      state.deleteModalCustomer.open = false;
      break;

    // Create Product
    case FETCH_CREATE_PRODUCT_PENDING:
      state.createProduct.pending = true;
      break;
    case FETCH_CREATE_PRODUCT_ERROR:
      state.createProduct.pending = false;
      state.createProduct.isError = true;
      break;
    case FETCH_CREATE_PRODUCT_SUCCESS:
      state.createProduct.pending = false;
      state.createProduct.isError = false;
      state.createProduct.message = action.payload.message;
      break;

    default:
      break;
  }

  return { ...state };
}

export default adminReducer;